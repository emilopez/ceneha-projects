Arduino Mini Pro
================

Esta plaquita es divina, tiene el mismo microcontrolador que el UNO, Atmega328P. 

Viene de 3.3v y de 5v, con un clock de 8 y 16MHz respectivamente. 

Algunos detalles que te pueden hacer perder tiempo y que como nosotros ya lo hemos perdido, queremos ahorrártelo:

- Para subir el código necesitás un adaptador de USB-RS232TTL, ya que esta plaquita no tiene un puerto USB como el UNO. Para esto se usan conversores CP2102 y necesitás tener instalado el driver en tu Sistema Operativo. En GNU/Linux ya viene y si te fijás bien, parece que el mismísimo Torvalds estuvo escribiendo parte del código (link)

- Cuando subas el código a la placa, elegí bien el modelo, 5 o 3.3v, 8 o 16MHz. Sino te va a fallar y vas a ver caracteres en una lengua incomprensible en la salida del serial, incluso estando a los mismos baudios.

- Una de 3.3v puede ser alimentada por 5v, se la banca, pero puede dar problemas si la dejás por mucho tiempo.

