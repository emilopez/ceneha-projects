#include <Honeywell_pressure_sensors.h>
#include <SPI.h>
//for sensor HSCDRRN015PDSA5
HPS hps(10, 14746.0, 1638.0, 15.0, -15.0); // cs is pin 10, outputMax=14746, outputMin=1638, pressureMax=15, pressureMin=-15

void setup() {
    Serial.begin(9600);
    SPI.begin();
    SPI.beginTransaction(SPISettings(800000, MSBFIRST, SPI_MODE0));
}

void loop() {
  Serial.println(hps.readPressure());
  delay(500);
}

