#include <SoftwareSPI.h>

// sck 6
// miso 7
// cs 5
SoftwareSPI spi(6,2,7,5);


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  spi.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
  spi.select();
  byte firstByte = spi.transfer(0x00);
  byte secondByte = spi.transfer(0x00);
  uint16_t data = (firstByte << 8) | secondByte;
  spi.deselect();
  Serial.println(transferFunction(data));
}

float transferFunction(uint16_t dataIn) {
    float outputMax = 14746.0; // 2 to the 14th power (from sensor's data sheet)
    float outputMin = 1638.0;
    float pressureMax = 15.0; // max 30 psi (from sensor's datea sheet)
    float pressureMin = -15.0;

    // transfer function: using sensor output to solve for pressure
    float pressure = pressureMin + (dataIn - outputMin) * (pressureMax - pressureMin) / (outputMax - outputMin);
    return pressure;
}
