#include <SPI.h>

int SSpin = 10;
int PowPin = 9;

void setup(){
    pinMode(PowPin, OUTPUT);
    digitalWrite(PowPin, LOW);
    
    Serial.begin(9600);
    SPI.begin();
    SPI.beginTransaction(SPISettings(800000, MSBFIRST, SPI_MODE0));
    
    pinMode(SSpin, OUTPUT);
    digitalWrite(SSpin, HIGH);
    pinMode(9, OUTPUT);
    digitalWrite(9, HIGH);
}

void loop(){
    byte firstByte;
    byte secondByte;

    // asserting/activate the slave this sensor by bringing CS pin low
    digitalWrite(SSpin, LOW);

    // here we're asking for our two bytes
    firstByte = SPI.transfer(0x00);
    secondByte = SPI.transfer(0x00);

    uint16_t bothBytes = (firstByte << 8) | secondByte;

    // de-assert /deactivate the slave (sensor)
    digitalWrite(SSpin, HIGH);
    
    Serial.println(transferFunction(bothBytes));
    delay(500);
}

float transferFunction(uint16_t dataIn) {
    float outputMax = 14746.0; // 2 to the 14th power (from sensor's data sheet)
    float outputMin = 1638.0;
    float pressureMax = 15.0; // max 30 psi (from sensor's datea sheet)
    float pressureMin = -15.0;

    // transfer function: using sensor output to solve for pressure
    float pressure = pressureMin + (dataIn - outputMin) * (pressureMax - pressureMin) / (outputMax - outputMin);
    return pressure;
}
