#include <SD.h>
#include <SoftwareSPI.h>
//SoftwareSPI spi(SPI_SCK,SPI_MOSI,SPI_MISO,SPI_CS);
SoftwareSPI spi(6,2,7,5);
File myFile;
const int CS = 10;

void setup(){
  delay(1000);
  spi.begin();
  Serial.begin(9600);
  pinMode(CS, OUTPUT);
  if (!SD.begin(CS)) {
    Serial.println("SD Card - initialization failed!");
  }else
    Serial.println("SD Card - initialization done");
  
}

void loop(){
  spi.select();
  byte firstByte = spi.transfer(0x00);
  byte secondByte = spi.transfer(0x00);
  uint16_t data = (firstByte << 8) | secondByte;
  spi.deselect();
  float psi = transferFunction(data);
  Serial.println(psi);
  
  //save data
   myFile = SD.open("testspi5.txt", FILE_WRITE);
   delay(1000);
   if (myFile) {
    myFile.println(psi);
  } else {
    Serial.println("error opening file");
  }
  
  myFile.close();
  
}

float transferFunction(uint16_t dataIn) {
    float outputMax = 14746.0; // 2 to the 14th power (from sensor's data sheet)
    float outputMin = 1638.0;
    float pressureMax = 15.0; // max 30 psi (from sensor's datea sheet)
    float pressureMin = -15.0;
    // transfer function: using sensor output to solve for pressure
    float pressure = pressureMin + (dataIn - outputMin) * (pressureMax - pressureMin) / (outputMax - outputMin);
    return pressure;
}
