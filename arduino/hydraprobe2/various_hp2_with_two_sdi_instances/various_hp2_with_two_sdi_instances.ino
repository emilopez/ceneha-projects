/*
 * 
 *  Tenemos dos instancias, sdiA y sdiB, la clave es que antes de usar cada uno hay que hacer
 *  el .begin(), eso lo activa y desactiva el resto. No puede haber mas de uno activo a la vez
 *  por eso es que no funciona usar los dos begin en el setup como usualmente hacemos
 * 
 * Luego, como siempre enviar "aM!" y luego "aD0!", "aD1!" y "aD2!"
*/

#include <SDI12.h>

#define DATAPINA 9         // change to the proper pin
#define DATAPINB 11

// en sdiA tengo 3 sensores con addr: 1, 2, 3
// en sdiB tengo 1 sensor con addr: 4

SDI12 sdiA(DATAPINA); 
SDI12 sdiB(DATAPINB); 
                          
void setup(){
    Serial.begin(9600); 
    
}

void loop(){
     String myCommand;
    // vamor a ir leyendo para los 3 sensores que estan con el cable de datos (AZUL) al pin 9
    //-------
    // sdiA
    //-------
    sdiA.begin();
    for (int addr = 1; addr<=3; addr++){
        // le avisamos que le vamos a pedir todo
        myCommand = String(addr) + "M!";
        sdiA.sendCommand(myCommand); 
        delay(300);                     // wait a while for a response
        String sdiResponse="";
        // concatenamos la respuesta que es un nro por ej 30029, 3 es la direccion va a demorar 2 segundos
        // y nos devuelve 9 variables
        while(sdiA.available()){    // write the response to the screen
            char c = sdiA.read();
            if ((c != '\n') && (c != '\r')){
                sdiResponse += c;
                delay(5);
            }
        }
        // vaciamos el buffer
        sdiA.flush();
        delay(1000);
        // ahora leemos D0
        //----------------
        myCommand = String(addr) + "D0!";
        sdiA.sendCommand(myCommand); 
        delay(300);                     // wait a while for a response
        sdiResponse+=";";
        while(sdiA.available()){    // write the response to the screen
            char c = sdiA.read();
            if ((c != '\n') && (c != '\r')){
                sdiResponse += c;
                delay(5);
            }
        }
        sdiA.flush();
        delay(1000); 
        // ahora leemos D1
        //----------------
        myCommand = String(addr) + "D1!";
        sdiA.sendCommand(myCommand); 
        delay(300);                     // wait a while for a response
        sdiResponse+=";";
        while(sdiA.available()){    // write the response to the screen
            char c = sdiA.read();
            if ((c != '\n') && (c != '\r')){
                sdiResponse += c;
                delay(5);
            }
        }
        sdiA.flush();
        delay(1000); 
        // ahora leemos D2
        //----------------
        myCommand = String(addr) + "D2!";
        sdiA.sendCommand(myCommand); 
        delay(300);                     // wait a while for a response
        sdiResponse+=";";
        while(sdiA.available()){    // write the response to the screen
            char c = sdiA.read();
            if ((c != '\n') && (c != '\r')){
                sdiResponse += c;
                delay(5);
            }
        }
        sdiA.flush();
        delay(1000); 
        Serial.print("sensor " + String(addr)+";");
        Serial.println(sdiResponse);
    }
    //-------
    // sdiB
    //-------
    sdiB.begin();
    // le avisamos que le vamos a pedir todo
    myCommand = "4M!";
    sdiB.sendCommand(myCommand); 
    delay(300);                     // wait a while for a response
    String sdiResponse="";
    // concatenamos la respuesta que es un nro por ej 30029, 3 es la direccion va a demorar 2 segundos
    // y nos devuelve 9 variables
    while(sdiB.available()){    // write the response to the screen
        char c = sdiB.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    // vaciamos el buffer
    sdiB.flush();
    delay(1000);
    // ahora leemos D0
    //----------------
    myCommand = "4D0!";
    sdiB.sendCommand(myCommand); 
    delay(300);                     // wait a while for a response
    sdiResponse+=";";
    while(sdiB.available()){    // write the response to the screen
        char c = sdiB.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    sdiB.flush();
    delay(1000); 
    // ahora leemos D1
    //----------------
    myCommand = "4D1!";
    sdiB.sendCommand(myCommand); 
    delay(300);                     // wait a while for a response
    sdiResponse+=";";
    while(sdiB.available()){    // write the response to the screen
        char c = sdiB.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    sdiB.flush();
    delay(1000); 
    // ahora leemos D2
    //----------------
    myCommand = "4D2!";
    sdiB.sendCommand(myCommand); 
    delay(300);                     // wait a while for a response
    sdiResponse+=";";
    while(sdiB.available()){    // write the response to the screen
        char c = sdiB.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    sdiB.flush();
    delay(1000); 
    Serial.print("sensor 4;");
    Serial.println(sdiResponse);
    
}
