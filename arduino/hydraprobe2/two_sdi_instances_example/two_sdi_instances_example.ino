#include <SDI12.h>

// uno en el pin 9 y el otro en el 11
SDI12 sdi1(9); 
SDI12 sdi2(11); 

/*
 * Le voy a consultar a cada uno el tipo de suelo con "aXS"
 * el sensor conectado al pin 9 tiene direccion 1 (a->1)
 * el sensor conectado al pin 11 tiene direccion 4 (a->4)
 * 
 * LA CLAVE ES USAR EL sdi1.begin() o sdi2.begin() segun corrsponda
 * antes de usarlo, pq eso desactiva las otras instancias
*/

void setup(){
    Serial.begin(9600); 
}

void loop(){
    //--------------------------------------
    // activa el sdi1 y desactiva los otros
    //--------------------------------------
    sdi1.begin(); 
    String myCommand = "1XS!";
    sdi1.sendCommand(myCommand); 
    delay(300);                     // wait a while for a response
    String sdiResponse="";
    while(sdi1.available()){    // write the response to the screen
        char c = sdi1.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    sdi1.flush();
    Serial.print("soil type: ");
    Serial.println(sdiResponse);
    delay(1000); 
    //--------------------------------------
    // activa el sdi2 y desactiva los otros
    //--------------------------------------
    sdi2.begin(); 
    myCommand = "4XS!";
    sdi2.sendCommand(myCommand); 
    delay(300);                     // wait a while for a response
    sdiResponse="";
    while(sdi2.available()){    // write the response to the screen
        char c = sdi2.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    sdi2.flush();
    Serial.print("soil type: ");
    Serial.println(sdiResponse);
    delay(1000); 
    
    
}
