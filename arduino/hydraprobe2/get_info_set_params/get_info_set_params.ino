#include <SDI12.h>


SDI12 mySDI12(8);

/*
 * - con "?I!" nos devuelve info del primer sensor que responda, si no sabemos la direccion
 * - get soil type command = "aXS"
 * - set soil type command = "aXS<soil>"
 *    donde a es el nro del address ID del sensor> 1, 2,3 ...
 *    <soil> es el nro que corresponde con el tipo de suelo:
 *          1 sand
 *          2 silt
 *          3 clay
 *          4 loam
*/



String cmdInfo = "?I!";      // con ? responde cualquiera sin importar su direccion, se reemplaza x el nro de dir si se lo conoce
String cmdSetSoil = "1XS4!"; // sensor con addr 1, seteo el tipo de suelo a 4 (loam)
String cmdGetSoil = "1XS!"; // sensor con addr 1, obtengo el tipo de suelo al que esta seteado

void setup(){
    Serial.begin(9600);
    //..............................................
    // bloque para prender alimentacion del DLOGGER
    //.............................................
      pinMode(9, OUTPUT); 
      digitalWrite(9,HIGH);
      delay(150);
    //....... .....................................
    mySDI12.begin();
}

void loop(){
    String cmd = cmdInfo;
    mySDI12.sendCommand(cmd);
    delay(300);                     // wait a while for a response
    String sdiResponse="";
    while(mySDI12.available()){    // write the response to the screen
        char c = mySDI12.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    mySDI12.flush();
    Serial.print("response: ");
    Serial.println(sdiResponse);
    delay(1000);


}
