#include <SDI12.h>

#define DATAPIN 9         // change to the proper pin
SDI12 mySDI12(DATAPIN); 

/*
'?' is a wildcard character which asks any and all sensors to respond
'I' indicates that the command wants information about the sensor
'!' finishes the command

112STEVENSW0000012.8ST4SN00188427

dir: 1
"1XS=GH!" setea soil moisture y temperatura
"1M!" : 
"1XM!"

H soil moisture
J soil coductivity (temp corrected)
F soil temp (celsius)

G soil temp (Farenheit)
O soil conductivity
K Real Dielectric Permittivity

M Imaginary Dielectric Permittivity
L Real Dielectric Permittivity (temp. corrected)
N Imaginary Dielectric Permittivity (temp. Corrected)

*/
String old_addr = "0";
String new_addr = "4";
String cmd      = old_addr + "A" + new_addr + "!";   // Se puede reemplazar I! por cualquiera de los comandos de las Tablas 2 y 3
                                                      // de acuerdo al resultado que se quiera obtener
String sdiResponse="";                                                      
void setup(){
    Serial.begin(9600); 
    mySDI12.begin(); 
    // chequeo la addr vieja
    Serial.print("Old addr: ");
    Serial.println(old_addr);
    mySDI12.sendCommand(old_addr+"I!"); 
    delay(300);
    sdiResponse="";  
    while(mySDI12.available()){    // write the response to the screen
        char c = mySDI12.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    mySDI12.flush();
    Serial.print("response: ");
    Serial.println(sdiResponse);

    Serial.print("New addr: ");
    Serial.println(new_addr);
    Serial.print("Command to modify addr: ");
    Serial.println(cmd);
    mySDI12.sendCommand(cmd); 
    delay(300);
    Serial.println("addr changed!!!");
    mySDI12.sendCommand(new_addr+"I!"); 
    delay(300);
    sdiResponse="";  
    while(mySDI12.available()){    // write the response to the screen
        char c = mySDI12.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    mySDI12.flush();
    Serial.print("response: ");
    Serial.println(sdiResponse);
     
}

void loop(){
      
}
