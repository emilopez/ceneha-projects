# Hydraprobe 2 con Arduino

## URGENTE, no en cualquier pin

- AtMega328p / Arduino Uno: All pins
- AtMega1284p / EnviroDIY Mayfly: All pins
- ATmega2560 / Arduino Mega or Mega 2560: 10, 11, 12, 13, 14, 15, 50, 51, 52, 53, A8 (62), A9 (63), A10 (64), A11 (65), A12 (66), A13 (67), A14 (68), A15 (69)
- AtMega32u4 / Arduino Leonardo or Adafruit Feather: 8, 9, 10, 11, 14 (MISO), 15 (SCK), 16 (MOSI)
- SAMD21G18 / Arduino Zero: All pins (except 4 on the zero)

## Descripción general

- Protocolo de comunicación SDI-12
- Se alimentan con 12v los sensores (entre 9 y 20v)
- La comunicación es a través de un puerto digital de Arduino
- Tiene 3 cables: V+, GND, DATA (azul)

Hay que armar un string con el comando a enviarle al sensor, luego leer la respuesta. Es necesario saber la dirección del sensor para poder consultarlo.

El comando SDI-12 se arma primero con un nro que es la direccion del sensor luego con una String que finaliza con !

**El primer comando que mandamos debería ser** "?I!"

- '?' is a wildcard character which asks any and all sensors to respond
- 'I' indicates that the command wants information about the sensor
- '!' finishes the command

En el caso previo va a funcionar con un único sensor, si hay varios puede responder cualquiera.

## Direcciones

Son un número: 1, 2, 3

Lo consultamos con el comando previo. Si tenemos mas de uno en paralelo, necesitamos cambiarles las direcciones para que no haya ninguna repetida, para eso usar: ``change_addrID_hp2.ino``

## Configuración por defecto

Primero debemos ver cómo está configurado el sensor, es decir, qué variables retorna y qué tipo de suelo tiene configurado.

### Setearle variables a medir
El Hydraprobe2 lo seteamos inicialmente para que devuelva todos los parametros que son 9, esto se hace con
"1XM=0!", las 9 variables vienen en el orden HJFGOKMLN que significa

H soil moisture
J soil coductivity (temp corrected)
F soil temp (celsius)

G soil temp (Farenheit)
O soil conductivity
K Real Dielectric Permittivity

M Imaginary Dielectric Permittivity
L Real Dielectric Permittivity (temp. corrected)
N Imaginary Dielectric Permittivity (temp. Corrected)

se las pide de a tres, con el comando D0 las primeras 3, luego D1 para la segunda tanda, y finalmente D2 para
la tercer tanda

antes de pedirle las variables con D0, D1 o D2 hay que pasarle "M!" para que sepa que le vamos a pedir.

### Setearle el tipo de suelo

- get soil type command = "aXS"
- set soil type command = "aXS<soil>"

donde a es el nro del address ID del sensor: 1, 2,3 ...
y ``<soil>`` es el nro que corresponde con el tipo de suelo: 1 sand, 2 silt, 3 clay, 4 loam

## Ejemplo 1

Veamos la identificación del sensor:

```cpp
#include <SDI12.h>
#define DATA_PIN 9

// Define the SDI-12 bus
SDI12 mySDI12(DATA_PIN);
String myCommand = "?I!";

void setup(){
  Serial.begin(9600);
  mySDI12.begin();
  delay(500); // allow things to settle
}

void loop(){
    mySDI12.sendCommand(myCommand);
    delay(300);                     // wait a while for a response
    String sdiResponse="";
    while(mySDI12.available()){
        char c = mySDI12.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    mySDI12.flush();
    Serial.println(sdiResponse);
    delay(1000); // un segundo de espera dsp del flush
}
```

## Ejemplo 2

Veamos como leer los valores que mide el sensor:

```cpp
#include <SDI12.h>

#define DATAPIN 9         // change to the proper pin
SDI12 mySDI12(DATAPIN);

String addr ="3";
String myCommand;
void setup(){
    Serial.begin(9600);
    mySDI12.begin();
}

void loop(){
    myCommand = addr + "M!";
    mySDI12.sendCommand(myCommand);
    delay(300);                     // wait a while for a response
    String sdiResponse="";
    while(mySDI12.available()){
        char c = mySDI12.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    mySDI12.flush();
    Serial.print("seteado: ");
    Serial.println(sdiResponse);
    delay(1000); // un segundo de espera dsp del flush

    //==== Los 9 datos los devuelve pidiendole D0, D1 y D2:
    //----D0
    myCommand = addr + "D0!";
    mySDI12.sendCommand(myCommand);
    delay(300);
    sdiResponse="";
    while(mySDI12.available()){
        char c = mySDI12.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    mySDI12.flush();
    Serial.print("D0: ");
    Serial.println(sdiResponse);
    delay(1000);

    //----D1
    myCommand = addr + "D1!";
    mySDI12.sendCommand(myCommand);
    delay(300);                     // wait a while for a response
    sdiResponse="";
    while(mySDI12.available()){    // write the response to the screen
        char c = mySDI12.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    mySDI12.flush();
    Serial.print("D1: ");
    Serial.println(sdiResponse);
    delay(1000);

    //----D2
    myCommand = addr + "D2!";
    mySDI12.sendCommand(myCommand);
    delay(300);
    sdiResponse="";
    while(mySDI12.available()){
        char c = mySDI12.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    mySDI12.flush();
    Serial.print("D2: ");
    Serial.println(sdiResponse);
    delay(1000);
}
```

## Varias instancias sdi

**MUY IMPORTANTE**, no usamos el ``.begin()`` dentro de la función ``setup()`` sino antes de usar cada sensor. Esto es necesario porque al hacerlo pone activo al la instancia que vamos a usar y deshabilita las otras

Acá lo explican:
- https://github.com/EnviroDIY/Arduino-SDI-12/wiki/6.-Using-more-than-one-SDI-12-object.

### Ejemplo de dos instancias

```cpp
#include <SDI12.h>

// uno en el pin 9 y el otro en el 11
SDI12 sdi1(9);
SDI12 sdi2(11);

/*
 * Le voy a consultar a cada uno el tipo de suelo con "aXS"
 * el sensor conectado al pin 9 tiene direccion 1 (a->1)
 * el sensor conectado al pin 11 tiene direccion 4 (a->4)
 *
 * LA CLAVE ES USAR EL sdi1.begin() o sdi2.begin() segun corrsponda
 * antes de usarlo, pq eso desactiva las otras instancias
*/

void setup(){
    Serial.begin(9600);
}

void loop(){
    //--------------------------------------
    // activa el sdi1 y desactiva los otros
    //--------------------------------------
    sdi1.begin();
    String myCommand = "1XS!";
    sdi1.sendCommand(myCommand);
    delay(300);                     // wait a while for a response
    String sdiResponse="";
    while(sdi1.available()){    // write the response to the screen
        char c = sdi1.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    sdi1.flush();
    Serial.print("soil type: ");
    Serial.println(sdiResponse);
    delay(1000);
    //--------------------------------------
    // activa el sdi2 y desactiva los otros
    //--------------------------------------
    sdi2.begin();
    myCommand = "4XS!";
    sdi2.sendCommand(myCommand);
    delay(300);                     // wait a while for a response
    sdiResponse="";
    while(sdi2.available()){    // write the response to the screen
        char c = sdi2.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    sdi2.flush();
    Serial.print("soil type: ");
    Serial.println(sdiResponse);
    delay(1000);


}
```
