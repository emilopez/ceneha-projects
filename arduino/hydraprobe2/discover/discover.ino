


#include <SDI12.h>

#define DATA_PIN 9         // The pin of the SDI-12 data bus

// Define the SDI-12 bus
SDI12 mySDI12(DATA_PIN);
String myCommand = "?I!";


void setup(){
  Serial.begin(9600);
  while(!Serial);

  Serial.println("Opening SDI-12 bus...");
  mySDI12.begin();
  delay(500); // allow things to settle
}

void loop(){

    mySDI12.sendCommand(myCommand);
    delay(300);                     // wait a while for a response
    String sdiResponse="";
    while(mySDI12.available()){
        char c = mySDI12.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    mySDI12.flush();
    Serial.print("seteado: ");
    Serial.println(sdiResponse);
    delay(1000); // un segundo de espera dsp del flush

}
