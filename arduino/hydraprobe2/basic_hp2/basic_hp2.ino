#include <SDI12.h>

#define DATAPIN 9         // change to the proper pin
SDI12 mySDI12(DATAPIN); 

/*
'?' is a wildcard character which asks any and all sensors to respond
'I' indicates that the command wants information about the sensor
'!' finishes the command

112STEVENSW0000012.8ST4SN00188427

dir: 1
"1XS=GH!" setea soil moisture y temperatura
"1M!" : 
"1XM!"
"1XM=0!" setea para que devuelva las 9 variables HJFGOKMLN

H soil moisture
J soil coductivity (temp corrected)
F soil temp (celsius)

G soil temp (Farenheit)
O soil conductivity
K Real Dielectric Permittivity

M Imaginary Dielectric Permittivity
L Real Dielectric Permittivity (temp. corrected)
N Imaginary Dielectric Permittivity (temp. Corrected)

*/

// PRIMERO PASAR XM=0 para setear al default la cantidad de parametros
// Luego M! para ver que te los devuelva

String addr ="3";
String myCommand;                           // de acuerdo al resultado que se quiera obtener
void setup(){
    Serial.begin(9600); 
    mySDI12.begin(); 
}

void loop(){
    myCommand = addr + "M!";
    mySDI12.sendCommand(myCommand); 
    delay(300);                     // wait a while for a response
    String sdiResponse="";
    while(mySDI12.available()){    // write the response to the screen
        char c = mySDI12.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    mySDI12.flush();
    Serial.print("seteado: ");
    Serial.println(sdiResponse);
    delay(1000); 
    
    //----D0
    myCommand = addr + "D0!";
    mySDI12.sendCommand(myCommand); 
    delay(300);                     // wait a while for a response
    sdiResponse="";
    while(mySDI12.available()){    // write the response to the screen
        char c = mySDI12.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    mySDI12.flush();
    Serial.print("D0: ");
    Serial.println(sdiResponse);
    delay(1000); 
    
    //----D1
    myCommand = addr + "D1!";
    mySDI12.sendCommand(myCommand); 
    delay(300);                     // wait a while for a response
    sdiResponse="";
    while(mySDI12.available()){    // write the response to the screen
        char c = mySDI12.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    mySDI12.flush();
    Serial.print("D1: ");
    Serial.println(sdiResponse);
    delay(1000); 
    //----D2
    myCommand = addr + "D2!";
    mySDI12.sendCommand(myCommand); 
    delay(300);                     // wait a while for a response
    sdiResponse="";
    while(mySDI12.available()){    // write the response to the screen
        char c = mySDI12.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    mySDI12.flush();
    Serial.print("D2: ");
    Serial.println(sdiResponse);
    delay(1000); 
    
    
}
