/*
 * 
 * El comando SDI-12 se arma primero con un nro que es la direccion del sensor
 * luego con una String que finaliza con !
 * 
 * si por ejemplo mandamos "?I!" 
'?' is a wildcard character which asks any and all sensors to respond
'I' indicates that the command wants information about the sensor
'!' finishes the command

El Hydraprobe2 lo seteamos inicialmente para que devuelva todos los parametros que son 9, esto se hace con
"1XM=0!", las 9 variables vienen en el orden HJFGOKMLN que significa

H soil moisture
J soil coductivity (temp corrected)
F soil temp (celsius)

G soil temp (Farenheit)
O soil conductivity
K Real Dielectric Permittivity

M Imaginary Dielectric Permittivity
L Real Dielectric Permittivity (temp. corrected)
N Imaginary Dielectric Permittivity (temp. Corrected)

se las pide de a tres, con el comando D0 las primeras 3, luego D1 para la segunda tanda, y finalmente D2 para 
la tercer tanda

antes de pedirle las variables con D0, D1 o D2 hay que pasarle "M!" para que sepa que le vamos a pedir

*/

#include <SDI12.h>

#define DATAPIN 8         // change to the proper pin

SDI12 mySDI12(DATAPIN); 
                          
void setup(){
    Serial.begin(9600); 
    mySDI12.begin(); 
}

void loop(){
     String myCommand;
    // vamor a ir leyendo para los 3 sensores que estan con el cable de datos (AZUL) al pin 9
    for (int addr = 1; addr<=3; addr++){
        // le avisamos que le vamos a pedir todo
        myCommand = String(addr) + "M!";
        mySDI12.sendCommand(myCommand); 
        delay(300);                     // wait a while for a response
        String sdiResponse="";
        // concatenamos la respuesta que es un nro por ej 30029, 3 es la direccion va a demorar 2 segundos
        // y nos devuelve 9 variables
        while(mySDI12.available()){    // write the response to the screen
            char c = mySDI12.read();
            if ((c != '\n') && (c != '\r')){
                sdiResponse += c;
                delay(5);
            }
        }
        // vaciamos el buffer
        mySDI12.flush();
        delay(1000);
        // ahora leemos D0
        //----------------
        myCommand = String(addr) + "D0!";
        mySDI12.sendCommand(myCommand); 
        delay(300);                     // wait a while for a response
        sdiResponse+=";";
        while(mySDI12.available()){    // write the response to the screen
            char c = mySDI12.read();
            if ((c != '\n') && (c != '\r')){
                sdiResponse += c;
                delay(5);
            }
        }
        mySDI12.flush();
        delay(1000); 
        // ahora leemos D1
        //----------------
        myCommand = String(addr) + "D1!";
        mySDI12.sendCommand(myCommand); 
        delay(300);                     // wait a while for a response
        sdiResponse+=";";
        while(mySDI12.available()){    // write the response to the screen
            char c = mySDI12.read();
            if ((c != '\n') && (c != '\r')){
                sdiResponse += c;
                delay(5);
            }
        }
        mySDI12.flush();
        delay(1000); 
        // ahora leemos D2
        //----------------
        myCommand = String(addr) + "D2!";
        mySDI12.sendCommand(myCommand); 
        delay(300);                     // wait a while for a response
        sdiResponse+=";";
        while(mySDI12.available()){    // write the response to the screen
            char c = mySDI12.read();
            if ((c != '\n') && (c != '\r')){
                sdiResponse += c;
                delay(5);
            }
        }
        mySDI12.flush();
        delay(1000); 
        Serial.print("sensor " + String(addr)+";");
        Serial.println(sdiResponse);
    }
}
