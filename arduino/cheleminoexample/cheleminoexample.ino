#include <Chelemino.h>

Chelemino datalogger(9,10);

void setup() {
    Serial.begin(9600);
}

void loop() {
    delay(2000);
    datalogger.wakeUp();
    Serial.print("ON");
    delay(2000);
    datalogger.deepSleep();
    Serial.print("OFF");
}

