#include <Wire.h>
#include <LiquidCrystal_I2C.h>


LiquidCrystal_I2C lcd(0x3f, 16, 2);

void setup(){
	lcd.begin();
	lcd.backlight();
        Wire.begin();
	Serial.begin(9600);
        
}

void loop(){
        lcd.noAutoscroll();
        
        lcd.setCursor(0,0);
        char sms[] = "flisol.lugli.org.ar";
        for (int i=0;i<3;i++){
          lcd.print("FLISOL Santa Fe");
          delay(1000);
          lcd.clear();
          delay(500);
        }
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Venite a DEMOS!");
        delay(1000);
        lcd.setCursor(0,1);
        lcd.print("9 de Julio 2239");
        delay(1000);
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Instalar");
        delay(1000);
        lcd.setCursor(0,1);
        lcd.print("Libertad!");
        delay(1000);
        lcd.clear();
        lcd.print("Inscribite ->");
        delay(1000);
        lcd.setCursor(16,0);      
        lcd.autoscroll();
        delay(500);
        for (int i=0;i<sizeof(sms)-1;i++){
          lcd.print(sms[i]);
          delay(500);
        }
        
        
}

