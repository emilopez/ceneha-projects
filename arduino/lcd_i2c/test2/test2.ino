//para sensor BMP183
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP183.h>
//para sensor SD, RTC y LCD
#include <Wire.h>
#include "Sodaq_DS3231.h"
#include <SD.h>
#include <LiquidCrystal_I2C.h>

#define BMP183_CLK  13
#define BMP183_SDO  12  // AKA MISO
#define BMP183_SDI  11  // AKA MOSI
#define BMP183_CS   8   // chip-select pin, use any pin!
Adafruit_BMP183 bmp = Adafruit_BMP183(BMP183_CLK, BMP183_SDO, BMP183_SDI, BMP183_CS);

LiquidCrystal_I2C lcd(0x3f, 16, 2);

void setup(){
    Serial.begin(9600);
    lcd.begin();
    if(!bmp.begin())
  {
    /* There was a problem detecting the BMP183 ... check your connections */
    Serial.print("Ooops, no BMP183 detected ... Check your wiring!");
    while(1);
  }
}
void loop(){
    //lcd.begin();
    int atm = bmp.getPressure() / 100;
    float temp = bmp.getTemperature();
    String dt = "2017-04-17 16:47:48";
    
    Serial.print(dt); 
    Serial.print("\t"); Serial.print(atm);
    Serial.print("\t"); Serial.println(temp);
    lcd.clear();
    
    lcd.setCursor(0,0); lcd.print(dt); 
    lcd.setCursor(0,1); 
    lcd.print(atm);lcd.print(" ");
    lcd.print(temp);
    
    delay(1000);
}
