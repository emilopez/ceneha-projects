#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP183.h>

// For hardware SPI:
// Connect SCK to SPI Clock, SDO to SPI MISO, and SDI to SPI MOSI
// See  http://arduino.cc/en/Reference/SPI for your Arduino's SPI pins!
// On UNO, Clock is #13, SDO/MISO is #12 and SDI/MOSI is #11

// You can also use software SPI and define your own pins!
#define BMP183_CLK  13
#define BMP183_SDO  12  // AKA MISO
#define BMP183_SDI  11  // AKA MOSI

// You'll also need a chip-select pin, use any pin!
#define BMP183_CS   8

// initialize with hardware SPI
//Adafruit_BMP183 bmp = Adafruit_BMP183(BMP183_CS);
// or initialize with software SPI and use any 4 pins
Adafruit_BMP183 bmp = Adafruit_BMP183(BMP183_CLK, BMP183_SDO, BMP183_SDI, BMP183_CS);

void setup(void){
  Serial.begin(9600);
  Serial.println("BMP183 Pressure Sensor Test"); Serial.println("");
  
  /* Initialise the sensor */
  if(!bmp.begin()){
    /* There was a problem detecting the BMP183 ... check your connections */
    Serial.print("Ooops, no BMP183 detected ... Check your wiring!");
    while(1);
  }
}
void loop(void){
    /* Display atmospheric pressue in Pascals */
    Serial.print("Presion:    ");
    Serial.print(bmp.getPressure() / 100);
    Serial.println(" hPa");

    /* First we get the current temperature from the BMP085 */
    float temperature;
    temperature = bmp.getTemperature();
    Serial.print("Temperatura: ");
    Serial.print(temperature);
    Serial.println(" C");
    /*
    Serial.print("Altitude:    "); 
    Serial.print(bmp.getAltitude(seaLevelPressure)); 
    Serial.println(" m");
    Serial.println("");
    */
    delay(1000);
}
