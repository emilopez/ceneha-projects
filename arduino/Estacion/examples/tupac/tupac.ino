#include <SD.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <RTClib.h>
#include <DHT.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP183.h>

#define LOGGERPIN   10
#define DHTPIN      6 
#define DHTTYPE     DHT22
#define BMP183_CLK  5
#define BMP183_SDO  4  // AKA MISO
#define BMP183_SDI  3  // AKA MOSI
#define BMP183_CS   2

LiquidCrystal_I2C lcd(0x3f, 16, 2);
RTC_DS1307 rtc;
DHT dht22(DHTPIN, DHTTYPE);
Adafruit_BMP183 bmp = Adafruit_BMP183(BMP183_CLK, BMP183_SDO, BMP183_SDI, BMP183_CS);

void setup(){
    Serial.begin(9600);
    lcd.begin();
    Wire.begin();
    rtc.begin();
    dht22.begin();
    bmp.begin();

    if (!rtc.isrunning()) {
        Serial.println(F("Error RTC"));
        lcd.print(F("Error RTC"));
    }
    //rtc.adjust(DateTime(__DATE__, __TIME__));
    pinMode(LOGGERPIN, OUTPUT);

    if (!SD.begin(LOGGERPIN)) {
        Serial.println(F("Error SD"));
        lcd.print(F("Error SD"));
        return;
    }
    Serial.println(F("Inicio OK..."));
    lcd.print(F("Inicio OK..."));
}

void loop(){
    show_timestamp();
    delay(2000);
    byte hum = dht22.readHumidity();
    float temp = dht22.readTemperature();
    float pATM = bmp.getPressure() / 100.0; // hPa
    float tempBMP = bmp.getTemperature();
    
    for (byte i=0; i < 4; i++){
      show_data(hum, temp, pATM, tempBMP);
      delay(1000);
    }
    if (rtc.now().minute() % 15 == 0){
        lcd.clear();
        lcd.print("guardando...");
        save_data(hum, temp, pATM, tempBMP);
        delay(2000);
    }

}

String timestamp(){
    // retorna string AAAA-MM-DD HH:MM:SS
    String ts;
    DateTime t = rtc.now();
    ts = String(t.year())+"-";
    ts += twodigits(t.month())+"-";
    ts += twodigits(t.day())+" ";
    ts += twodigits(t.hour())+":";
    ts += twodigits(t.minute())+":";
    ts += twodigits(t.second());
    return ts;
}

void show_timestamp(){
     String ts = timestamp();
    // timestamp x lcd
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print(ts.substring(8,10) + "-" + ts.substring(5,7) + "-" + ts.substring(0,4));
    lcd.setCursor(0,1);
    lcd.print(ts.substring(11));
}

void show_data(byte &hum, float &temp,  float &pATM, float &tempBMP){
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("H:");
    lcd.print(hum);
    lcd.print("%");
    lcd.print(" T1:");
    lcd.print(temp,1);
    lcd.print("C");
    lcd.setCursor(0,1);
    lcd.print("P:");
    lcd.print(pATM,1);
    lcd.print(" T2:");
    lcd.print(tempBMP,1);
    
}

void save_data(byte &hum, float &temp, float &pATM, float &tempBMP){
    // Usa o crea el archivo AAAMMDD.CSV y agrega el timestamp y medicion
    char fname[13];
    String sfname = timestamp();
    sfname = sfname.substring(0,4) + sfname.substring(5,7) + sfname.substring(8,10) + ".CSV";
    sfname.toCharArray(fname,15);
    File archi = SD.open(fname, FILE_WRITE);
    if (archi){
        archi.print(timestamp());
        archi.print(";");
        archi.print(hum);
        archi.print(";");
        archi.print(temp);
        archi.print(";");
        archi.print(pATM);
        archi.print(";");
        archi.println(tempBMP);
        archi.close();    
    }else
      Serial.println("error archivo");
}
String twodigits(byte val){
    if (val<10)
        return "0" + String(val);
    return String(val);
}
