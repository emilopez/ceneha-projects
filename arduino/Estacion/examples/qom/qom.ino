#include <WiFiEsp.h>
#include <Wire.h>
#include <SD.h>
#include <SPI.h>
#include "Sodaq_DS3231.h"

#ifndef HAVE_HWSERIAL1
#include "SoftwareSerial.h"
SoftwareSerial esp8266(3, 2);
SoftwareSerial mb7092(7, 8, true);  // (RX, TX)
#endif

#define POWWIFI 6   // Power pin WiFi
#define POWSD   9   // Power pin SD y RTC
#define CS      10  // Pin chip select SD

WiFiEspClient clienteWeb;

// prototipos de funciones
// =======================
unsigned short readSensorSerial();
void connectWIFI();
void upload(short &dist);
String get_timestamp();
void savedata(short &dist, String &timestamp);

void setup(){
    // Power lines UP!
    // ===============
    pinMode(POWWIFI, OUTPUT);
    pinMode(POWSD, OUTPUT);
    digitalWrite(POWWIFI, HIGH);
    digitalWrite(POWSD, HIGH); 
    delay(100);
    
    // SD SPI PINs
    // ===========
    pinMode(CS, OUTPUT); 
    digitalWrite(CS, HIGH);
    
    // Init all
    // ========
    Serial.begin(9600);
    Wire.begin();
    //rtc.begin();
    esp8266.begin(9600);
    connectWIFI();
    mb7092.begin(9600);
    if (!SD.begin(CS))
        Serial.println("error SD init");
    //rtc.setDateTime(DateTime(__DATE__, __TIME__));
}
// Creating a function pointer to address 0 then calling it reboots the board.
void (*reboot)(void) = 0; 

void loop(){
    
    // Medicion
    // ========
    mb7092.listen();
    delay(200);
    short dist = readSensorSerial();
    //String timestamp = get_timestamp();
    
    // Almacenamiento
    // ==============
    //Serial.print(timestamp);
    Serial.print(" ");
    Serial.println(dist);  
    
    //savedata(dist, timestamp);
    delay(200);
    
    // Transmision
    // ===========
    
    esp8266.listen();   // escucha puerto serie
    delay(1000);
    if (!clienteWeb.connect("things.ubidots.com", 80)){
        Serial.print("fail ubidot");
        delay(3000); // DELAY CLAVE
        reboot();
    }else{
      delay(3000);
      upload(dist);   // sube datos 
    }
    delay(1000);
}


void connectWIFI(){
    WiFi.init(&esp8266);  // initialize ESP module
    delay(100);
    if (WiFi.status() == WL_NO_SHIELD)
        Serial.println(F("WiFi not present"));
    else{
        while (WiFi.begin("ceneha-oeste", "nuevooeste") != WL_CONNECTED)
            delay(100);
        delay(500);
    }
}

void upload(short &dist){
    // subida de datos una vez conectado a la web
    String content = "{\"nivel\": " + String(dist) + "}";
    clienteWeb.println(F("POST /api/v1.6/devices/yatch-club?token=A1E-f6DEddsoJnN3XEJxaQYT2hgFhjbcNH HTTP/1.1"));
    clienteWeb.println(F("Host: things.ubidots.com"));
    clienteWeb.println(F("Accept: */*"));
    clienteWeb.println("Content-Length: " + String(content.length()));
    clienteWeb.println(F("Content-Type: application/json;"));
    clienteWeb.println();
    clienteWeb.println(content);

//    unsigned long timeout = millis();
//    while (clienteweb.available() == 0) {
//      if (millis() - timeout > 5000) {
//        Serial.println(F("web Timeout"));
//        web.stop();
//        return;
//      }
//    }
    
    String out = "";
    while (clienteWeb.available()) {
        char c = clienteWeb.read();
        out += c;
        //Serial.write(c);
    }
    Serial.println(out);
    if (!clienteWeb.connected()) {
        Serial.println(F("Disconnecting"));
        clienteWeb.stop();
    }

}

String get_timestamp(){
    DateTime now = rtc.now();
    String t = String(now.year()) + "-";
    t += (now.month()  < 10)? "0" + String(now.month() )   + "-": String(now.month())  + "-";
    t += (now.date()   < 10)? "0" + String(now.date()  )   + " ": String(now.date())   + " ";
    t += (now.hour()   < 10)? "0" + String(now.hour()  )   + ":": String(now.hour())   + ":";
    t += (now.minute() < 10)? "0" + String(now.minute())   + ":": String(now.minute()) + ":";
    t += (now.second() < 10)? "0" + String(now.second())        : String(now.second());   
    return t;
}

void savedata(short &dist, String &timestamp){
    File archi;
    archi = SD.open("nuevo.TXT", FILE_WRITE);
    if (archi) {
        archi.print(timestamp);
        archi.print(';');
        archi.println(dist);
        Serial.println(F(" -> SD"));
    } else Serial.println(F(" x SD"));
    archi.close();  
}

unsigned short readSensorSerial(){
    char buffer[3];
    // flush and wait for a range reading
    for (byte s = 0; s<5; s++){
        mb7092.flush();
        while (!mb7092.available() || mb7092.read() != 'R');
        for (byte i = 0; i < 3; i++) {
            while (!mb7092.available());
            buffer[i] = mb7092.read();
        }
    }
    return atoi(buffer);
}
