Estación de Monitoreo
=====================

Diferentes modelos de estaciones de monitoreo incluyen sensores, módulos, shields, etc.

En examples/nombre se va a encontrar cada uno de los ejemplos utilizados, que a continuación se describen


tupac
-----

Estacioncita que almacena en SD y muestra en LCD parámetros ambientales cada 15 minutos.

El objetivo es registrar determinados valores que serán luego contrastados con los de una estación comercial. No es óptimo su consumo, obviamente, porque es para probar rápidamente.

**Componentes**

- Arduino UNO
- Shield comercial Datalogger (RTC + SD)
- Sensor DHT22 (humedad y temp)
- Sensor BMP183 (presión ATM y temp)
- Display 16x2 I2C
- Alimentación: batería 12v conectada a regulador de panel solar

**Bibliotecas usadas**

.. code:: cpp

    #include <SD.h>
    #include <Wire.h>
    #include <LiquidCrystal_I2C.h>
    #include <RTClib.h>
    #include <DHT.h>
    #include <SPI.h>
    #include <Adafruit_Sensor.h>
    #include <Adafruit_BMP183.h>

qom
---

**Componentes**

Datalogger propio que cuenta con 2 transistores que cortan la energía del módulo SD y RTC para bajar consumo. Está formado por :

- Mini Pro 8 MHz 3.3V
- Módulo RTC DS3231
- Módulo Catalex microSD 5v (regulador interno a 3.3v)
- Transistores conectados al PIN digital 6, en HIGH prenden SD y RTC.

En otra placa cuenta con:

- WiFi ESP8266 3.3v
- Sensor MB7092 3.3v
- Display LCD 16x2

**Bibliotecas usadas**

.. code:: cpp

    #include <WiFiEsp.h>
    #include <Wire.h>
    #include <SD.h>
    #include <SPI.h>
    #include "Sodaq_DS3231.h"
    #include <LiquidCrystal_I2C.h>

**Pines usados**

Sin contar Vcc y GND, para cada módulo se usan:

El Reloj es por I2C, la gloria de los protocolos seriales de comunicación.

====  ========
RTC   Mini Pro
====  ========
SDA    A4
SCL    A5
====  ========

El módulo de almacenamiento usa el protocolo SPI, pero **ojo**, si se van a usar mas de un dispositivo SPI, es necesario soldarle una resistencia entre la línea MISO y el Arduino.

==========  ============
Catalex SD    Mini Pro
==========  ============
MISO         12
MOSI         11
SCK          13
CS           10
==========  ============

La placa WiFi requiere dos pines usando la lib ``SoftwareSerial``. Lo que es Tx en  el módulo es Rx en el Mini Pro y viceversa. Obvio!

=======  =========
ESP8266  Mini Pro
=======  =========
Rx          2 (Tx)
Tx          3 (Rx)
=======  =========

Para el distanciómetro se usa un único pin digital. Si bien la instancia de la biblioteca ``SoftwareSerial`` requiere que se declaren dos pines (Rx y Tx), solo se usa uno, ya que el sensor todo el tiempo manda datos al pin del Arduino que se usa como Rx.

=======  =========
MB7092   Mini Pro
=======  =========
Tx          7 (Rx)
=======  =========

Los módulos se prenden activando los transistores usando los siguientes pines digitales para su habilitación.

========  =========
Modulo    Mini Pro
========  =========
SD y RTC   9
WiFi       6
========  =========


**Funcionamiento**

Los módulos RTC y SD se activan poniendo en HIGH en PIN 9. El shield WiFi se activa poniendo en HIGH el PIN 6.





Cada X minutos:

- Mide
- Guarda en SD: timestamp, distancia, altura
- Sube altura a Ubidot en XY intentos

