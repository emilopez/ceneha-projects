//para sensor BMP183
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP183.h>
//para sensor SD, RTC y LCD
#include <Wire.h>
#include "Sodaq_DS3231.h"
#include <SD.h>
#include <LiquidCrystal_I2C.h>
//para DS18B20
#include <DallasTemperature.h>
#include <OneWire.h> //Se importan las librerías

#define BMP183_CLK  7
#define BMP183_SDO  6  // AKA MISO
#define BMP183_SDI  5  // AKA MOSI
#define BMP183_CS   4   // chip-select pin, use any pin!
Adafruit_BMP183 bmp = Adafruit_BMP183(BMP183_CLK, BMP183_SDO, BMP183_SDI, BMP183_CS);


#define DSPIN 3 //Se declara el pin donde se conectará la DATA
 
OneWire ourWire(DSPIN); //Se establece el pin declarado como bus para la comunicación OneWire
DallasTemperature ds18b20(&ourWire); //Se instancia la librería DallasTemperature

LiquidCrystal_I2C lcd(0x3f, 16, 2);
const int CS = 10;
File archi;
char fn[13];

void setup(){
    Serial.begin(9600); Wire.begin(); rtc.begin(); lcd.begin(); ds18b20.begin();
    rtc.setDateTime(DateTime(__DATE__, __TIME__));
    pinMode(CS, OUTPUT);
    if (!SD.begin(CS) || !bmp.begin()) {
        Serial.println("SD or BMP failed!");
        lcd.print("SD/BMP failed");
        return;
    }
    Serial.println("SD OK");
    lcd.print("SD OK");
    String fname = get_timestamp();
    fname = fname.substring(0,4)+fname.substring(5,7)+fname.substring(8,10)+".txt";
    fname.toCharArray(fn,15);
}
void loop(){
    DateTime now = rtc.now();
    lcd.begin();
    
    int v = analogRead(0);
    int atm = bmp.getPressure() / 100;
    float temp = bmp.getTemperature();
    String dt = get_timestamp();
    ds18b20.requestTemperatures();
    float tempsuelo = ds18b20.getTempCByIndex(0);
    int lluvia = analogRead(1);
    
    Serial.print(dt); Serial.print("\t"); 
    Serial.print(v); Serial.print("\t"); 
    Serial.print(atm); Serial.print("\t"); 
    Serial.print(temp); Serial.print("\t"); 
    Serial.print(tempsuelo); Serial.print("\t"); 
    Serial.println(lluvia);
    
    lcd.clear(); lcd.setCursor(0,0); 
    lcd.print(dt.substring(11)); lcd.print(" "); lcd.print(tempsuelo);
    lcd.setCursor(0,1); 
    lcd.print(v); lcd.print(" ");
    lcd.print(atm); lcd.print(" "); 

    lcd.print(temp);
    
    if (rtc.now().minute()%30 == 0) {
        Serial.println("guardando...");
        archi = SD.open(fn, FILE_WRITE);
        if (archi) {
            archi.print(dt); archi.print("\t"); 
            archi.print(v); archi.print("\t"); 
            archi.print(atm); archi.print("\t"); 
            archi.print(temp); archi.print("\t"); 
            archi.print(tempsuelo);archi.print("\t"); 
            archi.println(lluvia); 
            lcd.clear();
            dt = get_timestamp();
            lcd.setCursor(0,0); lcd.print(dt.substring(11));
            lcd.setCursor(0,1); lcd.print("Guardado en SD");
        } else Serial.println("error opening file");
        archi.close();
    }
    delay(5000);
}

String get_timestamp(){
    String t;
    DateTime now = rtc.now();
    t = String(now.year()) + "-";
    if (now.month()<10) 
        t+="0";
    t += String(now.month()) + "-";
    if (now.date()<10) 
        t+="0";
    t += String(now.date()) + " ";
    if (now.hour()<10) 
        t+="0";
    t += String(now.hour()) + ":";
    if (now.minute()<10) 
        t+="0";
    t += String(now.minute()) + ":";
    if (now.second()<10) 
        t+="0";
    t += String(now.second());
    return t;
}
