#include "Sodaq_DS3231.h"
#define PowDL 9
String get_timestamp();
void setup() {
  Serial.begin(9600);
  pinMode(PowDL, OUTPUT);//para datalogger
  digitalWrite(PowDL,HIGH);//para datalogger
  rtc.begin();
  //rtc.setDateTime(DateTime(__DATE__, __TIME__));
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println("test");
  Serial.println(get_timestamp());
}

String get_timestamp(){
    DateTime now = rtc.now();
    String t = String(now.year()) + "-";
    t += (now.month()  < 10)? "0" + String(now.month() )   + "-": String(now.month())  + "-";
    t += (now.date()   < 10)? "0" + String(now.date()  )   + " ": String(now.date())   + " ";
    t += (now.hour()   < 10)? "0" + String(now.hour()  )   + ":": String(now.hour())   + ":";
    t += (now.minute() < 10)? "0" + String(now.minute())   + ":": String(now.minute()) + ":";
    t += (now.second() < 10)? "0" + String(now.second())        : String(now.second());   
    return t;
}
