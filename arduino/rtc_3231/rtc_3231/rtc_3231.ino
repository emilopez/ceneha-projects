#include <Wire.h>
#include "Sodaq_DS3231.h"
  
void setup () 
{
    Serial.begin(9600);
    Wire.begin();
 
    //RTC CONFIG
    rtc.begin();
    // para que tome la hora y fecha de la PC
    //rtc.setDateTime(DateTime(__DATE__, __TIME__));
    //rtc.setDateTime(dt); //Adjust date-time as defined 'dt' above 
    //Serial.println("setup start");
}
 
void loop () {
    //Serial.println("loop start");
    DateTime now = rtc.now(); //get the current date-time
    Serial.print(now.year());
    Serial.print('-');
    Serial.print(now.month());
    Serial.print('-');
    Serial.print(now.date());
    Serial.print(' ');
    Serial.print(now.hour());
    Serial.print(':');
    Serial.print(now.minute());
    Serial.print(':');
    Serial.print(now.second());
    Serial.println();
    delay(1000);
}
