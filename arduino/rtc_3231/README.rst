Módulo RTC DS3231
=================

Conexionado
-----------

Funciona con i2c, por lo que hay que conectar solamente 2 líneas además de la alimentación: SCL y SDA.

Bibliotecas
-----------

Usar la biblioteca de Sodaq_DS3231 e incluir en el mismo path del programa principal el rtc_ds3231.h que icnluye dentro:

.. code-block:: cpp

    #ifndef __rtc_3231_h_
    #define __rtc_3231_h_

    void parse_cmd(char *cmd, int cmdsize);

    #endif

Mas info sobre la lib: https://github.com/SodaqMoja/Sodaq_DS3231

Ejemplo
-------

.. code-block:: cpp
    :include: rtc_3231/rtc_3231.ino
