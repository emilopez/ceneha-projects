#include <TinyGPS++.h>
#include <SoftwareSerial.h>

/*
   This sample code show lat, lon, altitude, speed and datetime.
   It requires the use of SoftwareSerial, and assumes that you have a 9600-baud serial GPS 
   device hooked up on pins 4(rx) and 3(tx).
*/

static const int RXPin = 4, TXPin = 3;
static const uint32_t GPSBaud = 9600;
SoftwareSerial ss(RXPin, TXPin);
TinyGPSPlus gps;

void setup(){
  Serial.begin(9600);
  ss.begin(GPSBaud);
}

void loop(){

  /*
  // simplest version
  while (ss.available() > 0){
     if (gps.encode(ss.read())){
         displayInfo();
     }
  }
  */
  // improved version
  myDelay(1000);
  displayInfo();

  if (millis() > 5000 && gps.charsProcessed() < 10)
      Serial.println(F("No GPS data received: check wiring"));
}
void displayInfo(){ 
  // print lat, lon, altitude, speed
  if (gps.location.isValid())  {
    Serial.print(gps.location.lat(), 6); Serial.print(F(",")); Serial.print(gps.location.lng(), 6);
    Serial.print(F(",")); Serial.print(gps.altitude.meters()); Serial.print(F(",")); Serial.print(gps.speed.kmph());
    Serial.print(F(","));
  }else{
    Serial.print(F("INVALID "));
  }

  //print datetime
  if (gps.date.isValid())  {
    Serial.print(gps.date.year());
    Serial.print(F("-"));
    Serial.print(gps.date.month());
    Serial.print(F("-"));
    Serial.print(gps.date.day());
  }else{
    Serial.print(F("INVALID"));
  }

  Serial.print(F(" "));
  if (gps.time.isValid()){
      Serial.print(gps.time.hour());
      Serial.print(F(":"));
      Serial.print(gps.time.minute());
      Serial.print(F(":"));
      Serial.print(gps.time.second());
      Serial.print(F("."));
      Serial.print(gps.time.centisecond());
  } else {
      Serial.print(F("INVALID"));
  }
  Serial.println();
}

static void myDelay(unsigned long ms){
  unsigned long start = millis();
  do{
    while (ss.available())
      gps.encode(ss.read());
  } while (millis() - start < ms);
}
