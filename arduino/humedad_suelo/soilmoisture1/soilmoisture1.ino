#include <SD.h>
#include <SoftwareSPI2.h>
#include "Sodaq_DS3231.h"
#include <LowPower.h>

//rtc.setDateTime(DateTime(__DATE__, __TIME__));
/*
 * 
 */
#define DL_CS 10
#define PowDL 9

String get_timestamp();

void setup(){
    Serial.begin(9600);
    // wake up
    // -------
    Serial.println("prendiendo...");
    pinMode(DL_CS, OUTPUT);
    pinMode(PowDL, OUTPUT); 
    digitalWrite(PowDL,HIGH);
    delay(200);
    rtc.begin();
    delay(200);
    /*
    if (!SD.begin(DL_CS)) {
      Serial.println("SD failed!");
    }else
      Serial.println("SD OK");
    */
}

void loop(){
    delay(500);
    Serial.println("timestamp: ");
    String ts = get_timestamp();
    Serial.println(ts+" ");
}

String get_timestamp(){
    DateTime now = rtc.now();
    String t = String(now.year()) + "-";
    t += (now.month()  < 10)? "0" + String(now.month() )   + "-": String(now.month())  + "-";
    t += (now.date()   < 10)? "0" + String(now.date()  )   + " ": String(now.date())   + " ";
    t += (now.hour()   < 10)? "0" + String(now.hour()  )   + ":": String(now.hour())   + ":";
    t += (now.minute() < 10)? "0" + String(now.minute())   + ":": String(now.minute()) + ":";
    t += (now.second() < 10)? "0" + String(now.second())        : String(now.second());   
    return t;
}
