#include <DHT.h>

//DHT
#define DHTPIN 2
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE);

//BAT ultrasonic 
#define CM 1      //Centimeter
#define INC 0     //Inch
#define TP 8      //Trig_pin
#define EP 9      //Echo_pin

void setup(){
  pinMode(TP,OUTPUT);       // set TP output for trigger  
  pinMode(EP,INPUT);        // set EP input for echo
  Serial.begin(9600);      // init serial 9600
  dht.begin();
  Serial.println("-------------------BAT_Demo_Start---------------------------------");
  delay(2000);
}
 
void loop(){  
  //lee la temperatura medida con el DHT22
  float t = dht.readTemperature(); //Se lee la temperatura
 Serial.println(t);
  long microseconds = TP_init();
  Serial.print("ret=");      //
  Serial.println(microseconds);
  float distance_cm = Distance(microseconds, CM, t);
  Serial.print("Distance_CM = ");
  Serial.println(distance_cm);
  delay(3000);
}
 
float Distance(long time, int flag, float t)
{
  /*
 
  */
  float distance;
  if(flag)
  //distancia sin considerar la temperatura
   //distance = time /29 / 2  ;     // Distance_CM  = ((Duration of high level)*(Sonic :340m/s))/2
                                   //              = ((Duration of high level)*(Sonic :0.034 cm/us))/2
                                   //              = ((Duration of high level)/(Sonic :29.4 cm/us))/2
 //distancia considerando la temperatura
 //331.4m/s en el aire; por cada grado centígrado que sube 
 //la temperatura, la velocidad del sonido aumenta en 0,6 m/s
  distance = (331.4+0.6*t)*((time/10000)/2);                                   
  else
    distance = time / 74 / 2;      // INC
  return distance;
}
 
long TP_init()
{                     
  digitalWrite(TP, LOW);                    
  delayMicroseconds(2);
  digitalWrite(TP, HIGH);                 // pull the Trig pin to high level for more than 10us impulse 
  delayMicroseconds(10);
  digitalWrite(TP, LOW);
  long microseconds = pulseIn(EP,HIGH);   // waits for the pin to go HIGH, and returns the length of the pulse in microseconds
  return microseconds;                    // return microseconds
}
 
 

