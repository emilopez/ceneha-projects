Distanciómetro Ultrasónico BAT 
==============================

Posee cuatro pines que son los siguientes:

* VCC (3.3 a 5V)
* echoPin (Input)
* trigPin (Output)
* GND 


Como la temperatura puede incidir en el valor arrojado por el sensor, para calcular la distancia se utilizó una fórmula que considera la temperatura ambiente: distancia=((331.4+0.6*t)/10000)*(duration/2), donde 331.4 es la velocidad del sonido en el aire. Se divide por 10000 para que el resultado sea en cm/ms.

La distancia mínima que mide son 2cm y, si bien en las especificaciones se indica que la distancia máxima es 5m, pudimos comprobar que esto no se cumple ya que a partir de los 4m el valor arrojado por el sensor es 0. Pero solo funciona correctamente hasta los 2m 30cm y al aumentar la distancia el resultado varía en un rango de unos 6 o 7 cm y no se estabiliza nunca.

Cabe destacar también que el valor medido no se condice con la distancia real como se observa en la siguiente tabla. Al principio la diferencia es de 2cm hasta llegar a una diferencia de 75cm aproximadamente en los 2m 30cm.

.. figure:: Tabla1.png


Correlación
-----------

Contando con los datos de la Tabla1 y sabiendo que la ecuación de la recta es Y=a+b*X, podemos hallar a y b mediante el método de mínimos cuadrados.
También calculamos el coeficiente de correlación r.

Considerando:
X=distancia real
Y=distancia medida por el sensor

Se obtuvo:
a=-2.058
b=1.306
r=0.998 (r=1 indica correlación total entre X e Y)

Entonces, teniendo los valores a, b y el que entrega el sensor, se puede obtener la distancia real haciendo X=(Y-a)/b.
