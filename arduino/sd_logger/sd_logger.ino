#include <SD.h>

File myFile;
const int CS = 10;

void setup(){
  delay(1000);
  Serial.begin(9600);
  pinMode(CS, OUTPUT);
  if (!SD.begin(CS)) {
    Serial.println("SD Card - initialization failed!");
  }else
    Serial.println("SD Card - initialization done");
  
}

void loop(){
  //save data
   myFile = SD.open("testspi2.txt", FILE_WRITE);
    delay(1000);
   if (myFile) {
    myFile.println("prueba1;");
    myFile.println("prueba2;");
    Serial.println("Save OKKK!!");
  } else {
    Serial.println("error opening file");
  }
  
  myFile.close();
  
}
