/*
  Datalogger.h - Library for wrap RTC + SD breakouts & electronic stuff.
  Created by Emiliano P. Lopez, July 11, 2017.
  Released under GPLv3.
*/

#ifndef Datalogger_h
#define Datalogger_h

#include "Arduino.h"
#include "RTClib.h"

class Datalogger{
    private:
        // config attributes
        uint8_t     _sdpin;
        RTC_DS1307  _rtc;
        char _filenametype;
        
    public:
        Datalogger(uint8_t sdpin, char filenametype='d');
        void begin();
        
        //getters
        bool isRunning();
        String getTime();
        String getDate(bool format = 1);
        String getDateTime();
        String get2Digits(byte val);
        byte getMinute();
        //setters
        void setDateTimeFromPC();
        //actions
        void log(String data, char sep=';');
};
#endif
