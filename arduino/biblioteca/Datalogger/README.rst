Biblioteca Datalogger
=====================

Esta biblioteca funciona como un wrapper de un módulo reloj RTC y de almacenamiento
en memorias SD. 

El objetivo del diseño desarrollado es facilitar y hacer más limpio el código
en el programa cliente, de modo que las acciones frecuentemente utilizadas para
medir y registrar variables sean métodos de sencilla aplicación.

Requerimientos
--------------

La primera versión fue desarrollada para el shield datalogger XD-05, por lo que
es necesario tener instaladas las bibliotecas SD, Wire y RTCLib, generalmente 
incluídas de fábrica con la instalación del Arduino IDE. 

Uso
---

Descargar y almacenar la biblioteca en la ruta que lee arduino (generalmente en /home/user/Arduino/libraries)

Incluir e instanciar
''''''''''''''''''''

.. code:: cpp

    #include <Datalogger.h>
    Datalogger dl(10,'m');

A la instancia dl recibe 3 argumentos:


- El PIN usado para comunicarse con el módulo de la SD (generalmente el 10 digital),
- El formato del nombre del archivo usado para guardar los datos, acepta dos tipos:
    - 'd': un archivo por día, en formato YYYYMMDD.CSV (opción por defecto)
    - 'm': un archivo por mes, en formato YYYYMM.CSV
    
Inicialización
''''''''''''''

Se inicializa con el método ``.begin()`` y se corrobora su funcionamiento con 
el método ``isRunning()``.

.. code:: cpp

    void setup() {
      dl.begin();
      if (dl.isRunning()){
          Serial.println("OK");
      }else
          Serial.println("KO");
    }
    
El seteo de la hora debe realizarse durante el setup al corroborar su funcionamiento.
Se realiza con el método ``.setDateTimeFromPC()`` que toma la hora de la PC al momento
de realizar la compilación del código. 

**WARNING:** una vez seteada la hora correspondiente se debe quitar esta función, ya que si 
el equipo se reinicia tomará la hora que tuvo al momento de la compilación, que ya será
erronea. 

Métodos implementados
'''''''''''''''''''''

- ``log(string data, char sep=';')``: guarda en la SD la cadena almacenada en data anteponiendo el timestamp y separándolo mediante el caracter sep. 
- ``getDateTime()``: retorna el string del timestamp en formato YYYY-MM-DD HH:MM:SS
- ``getDate()``: retorna el string de la fecha en formato YYYY-MM-DD HH:MM:SS
- ``getTime()``: retorna el string de la hora en formato HH:MM:SS

A continuación un fragmento de código en el loop principal que almacena ciertos
valores constantes.

.. code:: cpp

    void loop() {
      // put your main code here, to run repeatedly:
      Serial.println(dl.getDateTime());
      dl.log("21.4; 44.5; 55; OK");
      delay(500);
    }
    
Ejemplo
'''''''

Un ejemplo sencillo se encuentra bajo en example/example.ino

ToDO 
----

- Incoporar tipo de almacenamiento debug, que guarde todo en un archivo debug.log
- Agregar método write() y writeln()
- Pensar métodos para guardar cada cierto período de tiempo
- PEnsar método exahustivo, para pruebas.
