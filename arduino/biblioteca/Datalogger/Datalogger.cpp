#include "Arduino.h"
#include "Datalogger.h"

#include <SD.h>
#include <Wire.h>
#include "RTClib.h"

Datalogger::Datalogger(uint8_t sdpin=10, char filenametype='d'){
    pinMode(sdpin, OUTPUT);
    _sdpin = sdpin;
    _filenametype = filenametype;
}

void Datalogger::begin(){
    Wire.begin(); // used by rtc
    _rtc.begin();
}

bool Datalogger::isRunning(){    
    return _rtc.isrunning() && SD.begin(_sdpin);
}
// ---------------------
// ------ GETTERS ------
// ---------------------
String Datalogger::getTime(){
    return getDateTime().substring(11);
}

String Datalogger::getDate(bool format = 1){
    // format: 
    //      1 = YYYY-MM-DD
    //      0 = DD-MM-YYYY
    String ts = getDateTime();
    if (format)
        return ts.substring(0,4)  + "-" +  ts.substring(5,7) + "-" + ts.substring(8,10);
    return  ts.substring(8,10) + "-" +  ts.substring(5,7) + "-" + ts.substring(0,4);
}
        
String Datalogger::getDateTime(){
    String ts;
    DateTime t = _rtc.now();
    ts = String(t.year())+"-";
    ts += get2Digits(t.month())+"-";
    ts += get2Digits(t.day())+" ";
    ts += get2Digits(t.hour())+":";
    ts += get2Digits(t.minute())+":";
    ts += get2Digits(t.second());
    return ts;
}

String Datalogger::get2Digits(byte val){
    // WARNING: only for date numbers
    if (val<10)
        return "0" + String(val);
    return String(val);

}
byte Datalogger::getMinute(){
    return _rtc.now().minute();
}

// ---------------------
// ------ SETTERS ------
// ---------------------

void Datalogger::setDateTimeFromPC(){
    //set datetime from a connected PC when the compiler is invoked
    // WARGING, if not connected bad time will be setted
    //__DATE__ string constant eleven characters like "Feb 12 1996"
    //__TIME__ string constant eight characters like "23:59:01"
    if (_rtc.isrunning())    
        _rtc.adjust(DateTime(__DATE__, __TIME__));
}

// ---------------------
// ------ ACTIONS ------
// ---------------------

void Datalogger::log(String data, char sep=';'){
    char fname[13];
    String sfname = getDate();
    if (_filenametype == 'd')
        sfname = sfname.substring(0,4) + sfname.substring(5,7) + sfname.substring(8,10) + ".CSV";
    else if (_filenametype == 'm')
        sfname = sfname.substring(0,4) + sfname.substring(5,7) + ".CSV";
    sfname.toCharArray(fname,15);
    
    File file = SD.open(fname, FILE_WRITE);
    if (file){
        file.println(getDateTime() + sep + data);
        file.close();
    }
}


