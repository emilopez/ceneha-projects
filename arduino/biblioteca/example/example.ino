#include <Datalogger.h>

Datalogger dl(10,'m');

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  dl.begin();
  if (dl.isRunning()){
      Serial.println("OK");
      //dl.setDateTimeFromPC();
  }else
      Serial.println("KO");
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println(dl.getDateTime());
  dl.log();
  delay(500);

}
