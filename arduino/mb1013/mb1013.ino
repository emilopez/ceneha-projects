#include <Wire.h> //utilizan una comunicación serie I2C
#include "RTClib.h" 
 
RTC_DS1307 RTC;
 
void setup () {
    Serial.begin(9600);
    Wire.begin();//Establece la velocidad de datos del bus I2C
    RTC.begin(); //Establece la velocidad de datos del RTC
 
  if (! RTC.isrunning()) { // saber si el reloj externo esta activo o no
    Serial.println("RTC is NOT running!");
    //Para sincronizar con el PC HACER ESTO UNA SOLA VEZ
    //RTC.adjust(DateTime(__DATE__, __TIME__));
  }
}
 
void loop () {
    DateTime now = RTC.now();
 
    Serial.print(now.year());
    Serial.print('/');
    Serial.print(now.month());
    Serial.print('/');
    Serial.print(now.day());
    Serial.print(' ');
    Serial.print(now.hour());
    Serial.print(':');
    Serial.print(now.minute());
    Serial.print(':');
    Serial.print(now.second());
    Serial.print('\t');
    Serial.println(analogRead(0));
 
    delay(1000);
}
