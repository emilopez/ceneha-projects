#include "Maxbotix.h" // uso de biblioteca externa
Maxbotix Sensor(6, Maxbotix::TX, Maxbotix::HRLV);
void setup(){
  Serial.begin(9600);   // velocidad del puerto serie
}
void loop(){
  float dist = Sensor.getRange();// lectura del sensor
  Serial.println(dist);          // salida por puerto serie
  delay(2000);                   // espera de 2 segundos
}

