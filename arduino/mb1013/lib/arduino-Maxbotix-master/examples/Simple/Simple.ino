/*
  Maxbotix simple test

  Instructions:
  - At least one of: (comment the appropriate code below)
    * PW is digital pin 8
    * TX is digital pin 6
    * AN is analog pin A0
  - Change code below according to your model (LV, XL and
  HRLV supported)

  Note:
  For convenience, the getRange method will always return centimeters.
  You can use convert fuctions to convert to another unit (toInches and
  toCentimeters are available)

*/
#include "Maxbotix.h"
Maxbotix rangeSensorTX(6, Maxbotix::TX, Maxbotix::HRLV);


void setup(){
  Serial.begin(9600);
}

void loop(){

  // TX
  //Serial.print(rangeSensorTX.getRange()*10.0);
  //Serial.print("mm");
  //Serial.println();
  
  unsigned long start;
  start = millis();
  Serial.print("TX (MEDIAN): ");
  Serial.print(rangeSensorTX.getRange());
  Serial.print("cm - ");
  Serial.print(millis() - start);
  Serial.print("ms - ");
  printArray(rangeSensorTX.getSample(), rangeSensorTX.getSampleSize());
  Serial.print(" - Highest Mode: ");
  Serial.print(rangeSensorTX.getSampleMode(true));
  Serial.print(" - Lowest Mode: ");
  Serial.print(rangeSensorTX.getSampleMode(false));
  Serial.print(" - Median: ");
  Serial.print(rangeSensorTX.getSampleMedian());
  Serial.print(" - Best: ");
  Serial.print(rangeSensorTX.getSampleBest());
  Serial.println();
  
  delay(2000);
}

void printArray(float* array, uint8_t array_size) {
  Serial.print("[");
  for (int i = 0; i < array_size; i++) {
    Serial.print(array[i]);
    if (i != array_size - 1) {
      Serial.print(", ");
    }
  }
  Serial.print("]");
}
