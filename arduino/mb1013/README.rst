Component Overview
------------------

-  Hoja de datos: Imagen:HRLV-Datasheet.pdf
-  Web: http://maxbotix.com/Ultrasonic_Sensors/MB1013.htm
-  Resolution of 1‑mm
-  10Hz reading rate
-  Internal temperature compensation
-  Cost-effective solution where precision range-findings are needed
-  Sensor component allows users to lower the cost of their systems
   without sacrificing performance
-  42kHz Ultrasonic sensor measures distance to objects
-  Read from all sensor outputs: Analog Voltage, RS232 or TTL Serial,
   and Pulse Width
-  Virtually no sensor dead zone, objects closer than 30 cm will
   typically range as 30 cm
-  Maximum range of 5000 mm (195 inches)
-  Tolerates outside noise sources
-  Operates from 2.5V-5.5V
-  Low 3.1mA average current requirement

Funcionamiento
--------------

El sensor puede funcionar con 3 protocolos de comunicación:

-  analógico,
-  serial (ttl o rs232) y,
-  pwm.

Analógico
~~~~~~~~~

Atención: en este caso la resolución máxima del sensor es 5mm, no
importa si usas un ADC de mas cantidad de bits, no lo hagas, no vale la
pena.

.. figure:: mb1013-analogico.png
    :width: 200 px

.. code:: cpp

    const int anPin1 = 0;
    long distance1;

    void setup() {
      Serial.begin(9600);  // sets the serial port to 9600
    }
    void loop() {
      print_all();
      delay(100); 
    }

    void print_all(){
      Serial.print("S1"); Serial.print(" "); Serial.print(analogRead(anPin1)*5); Serial.print("mm");
      Serial.println();
      delay(1000);
    }

Serie
~~~~~

Esta es la única forma de obtener 1mm de resolución. Hay dos ejemplos,
uno que me mandó el de Maxbotix y otro usando la lib
https://github.com/Diaoul/arduino-Maxbotix cuya explicación básica está
acá: http://playground.arduino.cc/Main/MaxSonar

.. figure:: mb1013-serial.png
    :width: 200 px

Ejemplo base de maxbotix
^^^^^^^^^^^^^^^^^^^^^^^^

El pin de salida del sensor va al puerto digital 0 serie Rx del Arduino.

.. code:: cpp

    #include <SoftwareSerial.h>

    #define txPin 1                                            //define pins used for software serial for sonar
    #define rxPin 0

    SoftwareSerial sonarSerial(rxPin, txPin, true);            //define serial port for recieving data, output from maxSonar is inverted requiring true to be set.
    boolean stringComplete = false;

    void setup(){
      Serial.begin(9600);                                      //start serial port for display
      sonarSerial.begin(9600);                                 //start serial port for maxSonar
      delay(500);                                              //wait for everything to initialize
    }

    void loop(){
      int range = EZread();
      if(stringComplete){
        stringComplete = false;                                //reset sringComplete ready for next reading
        Serial.println(range);                                 //delay for debugging
        delay(5000);
      }  
    }

    int EZread(){
      int result, index = 0;;
      char inData[6];                                          //char array to read data into
      sonarSerial.flush();                                     // Clear cache ready for next reading
      while (stringComplete == false) {
          if (sonarSerial.available()){
              char rByte = sonarSerial.read();                     //read serial input for "R" to mark start of data
              if(rByte == 'R'){
                //Serial.println("rByte set");
                while (index < 5){                                  //read next 4 characters for range from sensor
                  if (sonarSerial.available()){
                    inData[index] = sonarSerial.read(); 
                    //Serial.println(inData[index]);               //Debug line
                    index++;                                       // Increment where to write next
                  }
                }
                inData[index] = 0x00;                              //add a padding byte at end for atoi() function
              }
              rByte = 0;                                           //reset the rByte ready for next reading
              index = 0;                                           // Reset index ready for next reading
              stringComplete = true;                               // Set completion of read to true
              result = atoi(inData);                               // Changes string data into an integer for use
        }
      }
      return result;
    }

Ejemplo usando lib modificada
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

En este caso, el pin de salida del sensor va al puerto digital 6 del
Arduino.

Usando la lib, el código es este:

.. code:: cpp

    /*
      Maxbotix simple test

      Instructions:
      - At least one of: (comment the appropriate code below)
        * PW is digital pin 8
        * TX is digital pin 6
        * AN is analog pin A0
      - Change code below according to your model (LV, XL and
      HRLV supported)

      Note:
      For convenience, the getRange method will always return centimeters.
      You can use convert fuctions to convert to another unit (toInches and
      toCentimeters are available)

    */
    #include "Maxbotix.h"
    Maxbotix rangeSensorTX(6, Maxbotix::TX, Maxbotix::HRLV);
    void setup(){
      Serial.begin(9600);
    }

    void loop(){
      // TX
      //Serial.print(rangeSensorTX.getRange()*10.0);
      //Serial.print("mm");
      //Serial.println();
      
      unsigned long start;
      start = millis();
      Serial.print("TX (MEDIAN): ");
      Serial.print(rangeSensorTX.getRange());
      Serial.print("cm - ");
      Serial.print(millis() - start);
      Serial.print("ms - ");
      printArray(rangeSensorTX.getSample(), rangeSensorTX.getSampleSize());
      Serial.print(" - Highest Mode: ");
      Serial.print(rangeSensorTX.getSampleMode(true));
      Serial.print(" - Lowest Mode: ");
      Serial.print(rangeSensorTX.getSampleMode(false));
      Serial.print(" - Median: ");
      Serial.print(rangeSensorTX.getSampleMedian());
      Serial.print(" - Best: ");
      Serial.print(rangeSensorTX.getSampleBest());
      Serial.println();
      
      delay(2000);
    }

    void printArray(float* array, uint8_t array_size) {
      Serial.print("[");
      for (int i = 0; i < array_size; i++) {
        Serial.print(array[i]);
        if (i != array_size - 1) {
          Serial.print(", ");
        }
      }
      Serial.print("]");
    }

PWM
~~~

.. figure:: mb1013-pwm.png
    :width: 200 px

.. code:: cpp

    const int pwPin1 = 2; //this may be different depending on the Arduino being used, and the other PW pins being used.
    long sensor1, cm, inches;

    void setup () {
      Serial.begin(9600);
      pinMode(pwPin1, INPUT);
    }
    void loop () {
      read_sensor();
      printall();
      delay(1000); //make this match the refresh rate of the sensor
    }
    void read_sensor(){
      sensor1 = pulseIn(pwPin1, HIGH);
    }
    void printall(){         
      Serial.print("S1");
      Serial.print(" ");
      Serial.print(sensor1); //This can be made to match the measurement type you wish to have.
      Serial.println("mm"); //Typically will be the range reading increments.
    }
