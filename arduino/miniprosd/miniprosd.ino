#include <Wire.h>
#include <SD.h>
//#include <SPI.h>
 
//define global
File archi;
const int CS = 10;
 
void setup(){
    Serial.begin(9600);
    Wire.begin();
    pinMode(CS, OUTPUT); 
    if (!SD.begin(CS)) {
        Serial.println("SD failed!");
        return;
    }
    Serial.println("SD OK");
}
 
void loop(){
    archi = SD.open("test.txt", FILE_WRITE);
    if (archi) 
        archi.println("testiando como un campion");
    else
        Serial.println("error opening file");
    archi.close();
    delay(1000);
}
