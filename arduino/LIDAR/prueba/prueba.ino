/*------------------------------------------------------------------------------
  Connections:
  LIDAR-Lite 5 Vdc (red) to Arduino 5v
  LIDAR-Lite I2C SCL (green) to Arduino SCL
  LIDAR-Lite I2C SDA (blue) to Arduino SDA
  LIDAR-Lite Ground (black) to Arduino GND
  
  (Capacitor recommended to mitigate inrush current when device is enabled)
  680uF capacitor (+) to Arduino 5v
  680uF capacitor (-) to Arduino GND

  See the Operation Manual for wiring diagrams and more information:
  http://static.garmin.com/pumac/LIDAR_Lite_v3_Operation_Manual_and_Technical_Specifications.pdf

------------------------------------------------------------------------------*/
#include <SD.h>
#include <RTClib.h>
#include <Wire.h>
#include <LIDARLite.h>
#define LOGGERPIN 10

RTC_DS1307 rtc;
LIDARLite myLidarLite;

void setup(){
    Serial.begin(115200); // Initialize serial connection to display distance readings
    myLidarLite.begin(0, true); // Set configuration to default and I2C to 400 kHz
    myLidarLite.configure(0); // Change this number to try out alternate configurations
    Wire.begin();
    rtc.begin();
  
    if (!rtc.isrunning()){
        Serial.println(F("Error RTC"));
        return;
    }
    //rtc.adjust(DateTime(__DATE__, __TIME__));
  
    if (!SD.begin(LOGGERPIN)) {
        Serial.println(F("Error SD"));
        return;
    }
}

void loop(){
    String nro_prueba="0013";
    
    delay(250);
    int dist = myLidarLite.distance();
    char fname[13];
    String sfname = timestamp();

    Serial.print(sfname);
    Serial.print("\t");
    Serial.println(dist);
    
    sfname =  sfname.substring(5,7) + sfname.substring(8,10) + nro_prueba+".CSV";
    sfname.toCharArray(fname,15);
    File archi = SD.open(fname, FILE_WRITE);
    if (archi){
        archi.print(timestamp());
        archi.print(";");
        archi.println(dist);
        archi.close();    
    }else
      Serial.println("Error archivo");
      
}

String timestamp(){
    // retorna string AAAA-MM-DD HH:MM:SS
    RTC_DS1307 rtc;
    String ts;
    DateTime t = rtc.now();
    ts = String(t.year())+"-";
    ts += twodigits(t.month())+"-";
    ts += twodigits(t.day())+" ";
    ts += twodigits(t.hour())+":";
    ts += twodigits(t.minute())+":";
    ts += twodigits(t.second());
    return ts;
}

String twodigits(int val){
    if (val<10)
        return "0" + String(val);
    return String(val);
}
