#include <SD.h>
#include <RTClib.h>
#include <Wire.h>
#include <LIDARLite.h>
#include <LiquidCrystal_I2C.h>


#define LOGGERPIN 10

RTC_DS1307 rtc;
LIDARLite myLidarLite;
LiquidCrystal_I2C lcd(0x3f, 16, 2); // 0x3f

void setup(){
    Serial.begin(115200); // Initialize serial connection to display distance readings
    myLidarLite.begin(0, true); // Set configuration to default and I2C to 400 kHz
    myLidarLite.configure(0); // Change this number to try out alternate configurations
    Wire.begin();
    rtc.begin();
    lcd.begin(); 
    
    if (!rtc.isrunning()){
        Serial.println(F("Error RTC"));
        return;
    }
    //rtc.adjust(DateTime(__DATE__, __TIME__));
  
    if (!SD.begin(LOGGERPIN)) {
        Serial.println(F("Error SD"));
        return;
    }
}

void loop(){
    String nro_prueba="0014";
    
    delay(250);
    int dist = myLidarLite.distance();
    char fname[13];
    String sfname = timestamp();

    Serial.print(sfname);
    Serial.print("\t");
    Serial.println(dist);
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Prueba "+nro_prueba);
    lcd.setCursor(0,1);
    lcd.print(timestamp().substring(10,19));
    lcd.print(" ");
    lcd.print(dist);
    
    sfname =  sfname.substring(5,7) + sfname.substring(8,10) + nro_prueba+".CSV";
    sfname.toCharArray(fname,15);
    File archi = SD.open(fname, FILE_WRITE);
    if (archi){
        archi.print(timestamp());
        archi.print(";");
        archi.println(dist);
        archi.close();    
    }else
      Serial.println("Error archivo");
      
}

String timestamp(){
    // retorna string AAAA-MM-DD HH:MM:SS
    RTC_DS1307 rtc;
    String ts;
    DateTime t = rtc.now();
    ts = String(t.year())+"-";
    ts += twodigits(t.month())+"-";
    ts += twodigits(t.day())+" ";
    ts += twodigits(t.hour())+":";
    ts += twodigits(t.minute())+":";
    ts += twodigits(t.second());
    return ts;
}

String twodigits(int val){
    if (val<10)
        return "0" + String(val);
    return String(val);
}
