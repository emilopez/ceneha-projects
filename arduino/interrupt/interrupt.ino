
#include <TimerOne.h>

volatile bool sensing=false;

void setup(){
    Serial.begin(9600);
    Serial.print("Inicio OK...");
    Timer1.initialize(150000);         // initialize timer1 150ms
    Timer1.attachInterrupt(save_data); 
}

void loop(){
    
    Serial.println("no sensing");
    while (!sensing);
    sensing = false;

}

void save_data(){
    //funcion llamada cada vez q desborda el buffer del timer
    Serial.println("sensing");
    sensing = true;
   
}

