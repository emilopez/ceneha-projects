DS18B20
=======

Sensor de temperatura digital protegido para enterrar en suelo o medir temperatura en agua.


Interconexión de un Sensor
--------------------------

* Amarillo: DATA
* Rojo: VDD
* Negro: GND

Debe puentearse el cable DATA con VDD utilizando una resistencia de 4.7 KOhm.


Modos de uso
------------

Existen dos maneras de acceder a los datos que mide:

Modo 1
''''''

Leyendo directamente el dato, por ej: sensors.getTempCByIndex(0)

Modo 2
''''''

Accediendo al valor a través del identificador (como una MAC) del sensor, por ej: sensors.getTempC(Probe01), donde Probe01 es la dirección que identifica unívocamente cada sensor

Conexionado
-----------

En caso que uno cuente con varios sensores conectados en el mismo cable (en bus) es recomendable utilizar la segunda opción. La siguientes figuras muestras el esquema de conexión para uno, varios sensores y en el caso que esté conectado con otro sensor digital, como es el caso del DHT22.


.. figure:: ds18b20_esquema.png

.. figure:: ds18b20_varios_sensores.png

.. figure:: ds18b20-con-DHT22.png

Ejemplos
--------

En la carpeta *examples*, se pueden encontrar cada uno de estos ejemplos. El que está conectado con el sensor DHT22 se encuentra además para loggear la info en una memoria SD.

2 sensores y display lcd
''''''''''''''''''''''''



A: 0x28, 0xFF, 0x01, 0x2F, 0x44, 0x16, 0x03, 0x64
B: 0x28, 0xFF, 0xF5, 0xC9, 0x43, 0x16, 0x03, 0xE7
C: 0x28, 0xFF, 0x0B, 0xC8, 0x4C, 0x04, 0x00, 0xAC
D :0x28, 0xFF, 0x57, 0xFC, 0x6F, 0x14, 0x04, 0xA3

Descubrir identificador
-----------------------

Para saber la dirección que identifica al sensor se puede utilizar el siguiente programa:

.. code:: cpp

    //el PIN de datos va a la entrada digital 3 de arduino. Luego, guardar la dirección de salida.
    #include <OneWire.h>
    OneWire  ds(3);  // Connect your 1-wire device to pin 3

    void setup(void) {
      Serial.begin(9600);
      discoverOneWireDevices();
    }

    void discoverOneWireDevices(void) {
      byte i;
      byte present = 0;
      byte data[12];
      byte addr[8];

      Serial.print("Looking for 1-Wire devices...\n\r");
      while(ds.search(addr)) {
        Serial.print("\n\rFound \'1-Wire\' device with address:\n\r");
        for( i = 0; i < 8; i++) {
          Serial.print("0x");
          if (addr[i] < 16) {
            Serial.print('0');
          }
          Serial.print(addr[i], HEX);
          if (i < 7) {
            Serial.print(", ");
          }
        }
        if ( OneWire::crc8( addr, 7) != addr[7]) {
            Serial.print("CRC is not valid!\n");
            return;
        }
      }
      Serial.print("\n\r\n\rThat's it.\r\n");
      ds.reset_search();
      return;
    }

    void loop(void) {
      // nothing to see here
    }

Comentarios
-----------

Es un termómetro digital de alta precisión, entre 9 y 12 bits de temperatura en grados Celsius (el programador puede escoger la precisión deseada). Su temperatura operativa se encuentra entre -50 y 125 grados Celsius. La precisión, en el rango comprendido entre -10 y 85 grados es de ±0.5 grados.

Es económico, su interfaz de funcionamiento es sencilla y su uso es muy provechoso para proyectos que requieran mediciones precisas y confiables.

Se recomienda esperar 5 minutos para su estabilización.
