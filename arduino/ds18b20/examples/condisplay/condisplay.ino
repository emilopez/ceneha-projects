/*
Con el firmware previo se descubrió las direcciones de dos sensores:
    Sensor 1: 0x28, 0xFF, 0x0B, 0xC8, 0x4C, 0x04, 0x00, 0xAC
    Sensor 2: 0x28, 0xFF, 0xAE, 0xA1, 0x4E, 0x04, 0x00, 0x01 
    
*/
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#define ONE_WIRE_BUS_PIN 3
OneWire oneWire(ONE_WIRE_BUS_PIN);
 
DallasTemperature sensors(&oneWire);
 //LiquidCrystal_I2C lcd(0x27, 16, 2);
LiquidCrystal_I2C lcd(0x3f, 16, 2);
 
//DeviceAddress Probe01 = { 0x28, 0xFF, 0x01, 0x2F, 0x44, 0x16, 0x03, 0x64 }; 
//DeviceAddress Probe02 = { 0x28, 0xFF, 0xF5, 0xC9, 0x43, 0x16, 0x03, 0xE7 };
DeviceAddress Probe03 = { 0x28, 0xFF, 0x0B, 0xC8, 0x4C, 0x04, 0x00, 0xAC };
DeviceAddress Probe04 = { 0x28, 0xFF, 0x57, 0xFC, 0x6F, 0x14, 0x04, 0xA3 };
void setup(){
  // start serial port to show results
  lcd.begin(); 
  Wire.begin();
  Serial.begin(9600);
  Serial.print("Initializing Temperature Control Library Version ");
  Serial.println(DALLASTEMPLIBVERSION);
  // Initialize the Temperature measurement library
  sensors.begin();
  // set the resolution to 10 bit (Can be 9 to 12 bits .. lower is faster)
  //sensors.setResolution(Probe01, 10);
  //sensors.setResolution(Probe02, 10);
  sensors.setResolution(Probe03, 10);
  sensors.setResolution(Probe04, 10);
}
 
void loop(){
  delay(500);
  Serial.println();
  Serial.print("Number of Devices found on bus = ");  
  Serial.println(sensors.getDeviceCount());   
  Serial.print("Getting temperatures... ");  
  Serial.println();   
 
  // Command all devices on bus to read temperature  
  sensors.requestTemperatures();  
 
  //Serial.print("Probe 01 temperature is:   ");
  //Serial.print(sensors.getTempC(Probe01));
  //Serial.println();
 
  //Serial.print("Probe 02 temperature is:   ");
  //Serial.print(sensors.getTempC(Probe02));
  //Serial.println(); 

  Serial.print("Probe 03 temperature is:   ");
  Serial.print(sensors.getTempC(Probe03));
  lcd.setCursor(0,0);
  lcd.print("Temp 1: ");
  lcd.print(sensors.getTempC(Probe03));
  Serial.println(); 

  Serial.print("Probe 04 temperature is:   ");
  Serial.print(sensors.getTempC(Probe04));
  lcd.setCursor(0,1);
  lcd.print("Temp 2: ");
  lcd.print(sensors.getTempC(Probe04));
  Serial.println(); 
}
