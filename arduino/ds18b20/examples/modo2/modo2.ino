/*
Con el firmware previo se descubrió las direcciones de dos sensores:
    Sensor 1: 0x28, 0xFF, 0x0B, 0xC8, 0x4C, 0x04, 0x00, 0xAC
    Sensor 2: 0x28, 0xFF, 0xAE, 0xA1, 0x4E, 0x04, 0x00, 0x01 
    
*/

#include <OneWire.h>
 
//Get DallasTemperature Library here:  http://milesburton.com/Main_Page?title=Dallas_Temperature_Control_Library
#include <DallasTemperature.h>
 
/*-----( Declare Constants and Pin Numbers )-----*/
#define ONE_WIRE_BUS_PIN 2
 
/*-----( Declare objects )-----*/
// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(ONE_WIRE_BUS_PIN);
 
// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);
 
/*-----( Declare Variables )-----*/
 
 
DeviceAddress Probe01 = { 0x28, 0xFF, 0x0B, 0xC8, 0x4C, 0x04, 0x00, 0xAC }; 
DeviceAddress Probe02 = { 0x28, 0xFF, 0xAE, 0xA1, 0x4E, 0x04, 0x00, 0x01 };
 
 
 
void setup()   /****** SETUP: RUNS ONCE ******/
{
  // start serial port to show results
  Serial.begin(9600);
  Serial.print("Initializing Temperature Control Library Version ");
  Serial.println(DALLASTEMPLIBVERSION);
 
  // Initialize the Temperature measurement library
  sensors.begin();
 
  // set the resolution to 10 bit (Can be 9 to 12 bits .. lower is faster)
  sensors.setResolution(Probe01, 10);
  sensors.setResolution(Probe02, 10);
 
 
}
 
void loop()   /****** LOOP: RUNS CONSTANTLY ******/
{
  delay(1000);
  Serial.println();
  Serial.print("Number of Devices found on bus = ");  
  Serial.println(sensors.getDeviceCount());   
  Serial.print("Getting temperatures... ");  
  Serial.println();   
 
  // Command all devices on bus to read temperature  
  sensors.requestTemperatures();  
 
  Serial.print("Probe 01 temperature is:   ");
  Serial.print(sensors.getTempC(Probe01));
  Serial.println();
 
  Serial.print("Probe 02 temperature is:   ");
  Serial.print(sensors.getTempC(Probe02));
  Serial.println(); 
}
