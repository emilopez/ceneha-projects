#include <DHT.h>            
#include "RTClib.h"         
#include <SD.h>
#include <Wire.h>
 
#include <DallasTemperature.h>
#include <OneWire.h> 
 
#define DS18b20PIN 3        // Data Pin DS18b20
 
#define DHTPIN 2            // Data Pin DHT22
#define DHTTYPE DHT22       // Modelo DHT22 (hay otros DHT)                        
                            // Instancias:
RTC_DS1307 RTC;             //  RTC
DHT dht(DHTPIN, DHTTYPE);   //  DHT22
OneWire ourWire(DS18b20PIN); //Se establece el pin declarado como bus para la comunicación OneWire
DallasTemperature dstemp(&ourWire); //Se instancia la librería DallasTemperature
 
const int chipSelect = 10;  // Pin para SD
int prev_min = 0;
 
DeviceAddress Probe01 = { 0x28, 0xFF, 0x0B, 0xC8, 0x4C, 0x04, 0x00, 0xAC }; 
DeviceAddress Probe02 = { 0x28, 0xFF, 0xAE, 0xA1, 0x4E, 0x04, 0x00, 0x01 };
 
 
void setup(){
  dht.begin();              // init    
  Serial.begin(9600);
  Wire.begin();
  RTC.begin();
  dstemp.begin();
 
  dstemp.setResolution(Probe01,12);
  dstemp.setResolution(Probe02,12);
 
 
  if (! RTC.isrunning()) {
    Serial.println("RTC is NOT running!");
    //RTC.adjust(DateTime(__DATE__, __TIME__));
  }
  pinMode(10, OUTPUT);      // Para la SD
 
  if (!SD.begin(chipSelect)) {
    Serial.println("SD failed!");
    return;
  }
  Serial.println("SD init done");
  delay(3000);
}
 
void loop(){
 
    //Serial.print("Number of Devices found on bus = ");  
    //Serial.println(dstemp.getDeviceCount());        // cant. de sensores conectados
 
    dstemp.requestTemperatures(); //Prepara el sensor para la lectura
 
    DateTime now = RTC.now();
    int yyyy = now.year();
    int mm = now.month();
    int dd = now.day();
    int hh = now.hour();
    int mi = now.minute();
    int se = now.second();
 
    float hum = dht.readHumidity(); 
    float temp1 = dht.readTemperature();
    float temp2 = dstemp.getTempC(Probe01);
    float temp3 = dstemp.getTempC(Probe02);
 
    print_timestamp(yyyy,mm,dd,hh,mi,se); Serial.print('\t');
    Serial.print(hum);   Serial.print('\t');
    Serial.print(temp1); Serial.print('\t');
    Serial.print(temp2); Serial.print('\t');
    Serial.print(temp3); Serial.println();
 
    File archi = SD.open("temps.txt", FILE_WRITE);
 
    if (archi){
      if (mi % 2 == 0){
        if (prev_min != mi){
          prev_min = mi;
          archi.print(yyyy);
          archi.print('-');
          if (mm >= 0 && mm < 10) 
            archi.print('0');
          archi.print(mm);  
          archi.print('-');
          if (dd >= 0 && dd < 10) 
            archi.print('0');
          archi.print(dd);
          archi.print(' ');
          if (hh >= 0 && hh < 10) 
            archi.write('0');
          archi.print(hh);
          archi.print(':');
          if (mi >= 0 && mi < 10) 
            archi.write('0');
          archi.print(mi);
          archi.print(':');
          if (se >= 0 && se < 10) 
            archi.write('0');
          archi.print(se);  archi.print('\t');
          archi.print(hum); archi.print('\t');
          archi.print(temp1);archi.print('\t');
          archi.print(temp2);archi.print('\t');
          archi.print(temp3);archi.print('\t');          
          archi.println();
          Serial.println("data was saved");
        }
      }
      archi.close();
    }else {
      Serial.println("error opening file");
    }
    delay(5000);
}
 
void print_timestamp(int y,int m,int d,int hh,int mi,int se){
  Serial.print(y);
  Serial.print('-');
  print2digits(m);
  Serial.print('-');
  print2digits(d);
  Serial.print(' ');
  print2digits(hh);
  Serial.print(':');
  print2digits(mi);
  Serial.print(':');
  print2digits(se);
}
 
void print2digits(int number) {
  if (number >= 0 && number < 10) {
    Serial.write('0');
  }
  Serial.print(number);
}
