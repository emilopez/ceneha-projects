#include <DallasTemperature.h>
#include <OneWire.h> //Se importan las librerías
#define Pin 2 //Se declara el pin donde se conectará la DATA
 
OneWire ourWire(Pin); //Se establece el pin declarado como bus para la comunicación OneWire
DallasTemperature sensor(&ourWire); //Se instancia la librería DallasTemperature
 
void setup() {
  delay(1000);
  Serial.begin(9600);
  sensor.begin(); //Se inician los sensores
}
 
void loop() {
    sensor.requestTemperatures(); //Prepara el sensor para la lectura
    Serial.print(sensor.getTempCByIndex(0)); //Se lee e imprime la temperatura en grados Celsius
    Serial.println("C");
    delay(1000); //Se provoca un lapso de 1 segundo antes de la próxima lectura
}
