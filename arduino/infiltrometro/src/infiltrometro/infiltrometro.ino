#include <Arduino.h>

#include <Wire.h>           // Libs
#include "RTClib.h"         
#include <SD.h>
 
RTC_DS1307 RTC;
const int chipSelect = 10; 

#define SERIES_RESISTOR     560    // Value of the series resistor in ohms.    
char fn[13];                       // global var to filename yyyymmdd.txt
int op = 0;
int dt = 500;
void setup(){
    pinMode(chipSelect, OUTPUT);     // Para la SD
    pinMode(3, OUTPUT);              // Pin Digi. 3 de Salida: activa el relay q prende bomba
    
    Serial.begin(9600);
    Wire.begin();             
    RTC.begin();              
       
    if (! RTC.isrunning()) 
      Serial.println("RTC is NOT running!");
    if (!SD.begin(chipSelect)) {
      Serial.println("SD initialization failed!");
      Serial.println();
      return;
    }
    Serial.println("SD initialization done.");
    
    getFileName(fn);                                          // set fn with yyyymmdd.txt
    File archi;
    archi = SD.open(fn, FILE_WRITE);                         // create file called fn
    archi.println("Nuevo Experimento: " + getDateTime());    // each test with a timestamp header
    archi.println("======================================");
    archi.close();
    digitalWrite(3, HIGH); 
    
    Serial.println("Menu de Opciones");
    Serial.println("=================");
    Serial.println("[1] Calibracion");
    Serial.println("[2] Experimiento infiltracion");
    Serial.flush();                                          //flush all previous received and transmitted data
    while (Serial.available() == 0);
    op = Serial.parseInt();                                  //read int or parseFloat for ..float...
    Serial.flush();
    if (op == 1){
        Serial.println();
        Serial.println("Pasos para la calibracion");
        Serial.println("''''''''''''''''''''''''''");
        Serial.println("   i - Modificar el nivel de agua");
        Serial.println("  ii - Aguardar su estabilizacion");
        Serial.println(" iii - Ingresar altura h (valor numerico) y <enter>");
        Serial.println("  iv - Desconectar para finalizar");
    }else if (op == 2){
        Serial.println();
        Serial.println("Experimento infiltracion");
        Serial.println("'''''''''''''''''''''''''");
        Serial.println("Ingrese delta tiempo [miliseg]: ");
        Serial.flush(); 
        while (Serial.available() == 0);
        dt = Serial.parseInt();
    } else {
        Serial.println("Opcion incorrecta");
    }
    
}
  
void loop(){
    String timestamp;
    float raw_val;
    float h;
    
    if (op == 1){
        
        Serial.println(" Ingrese altura h y presione <enter>: ");
        while (Serial.available() == 0);
        h = Serial.parseFloat(); //read int or parseFloat for ..float...
        Serial.print("# 5 registros x nivel ingresado: ");
        Serial.print(h);
        Serial.println(" cm");
        
        for (int i=1;i<=5;i++){
            // Por cada nivel de agua ingresado toma 5 registros, uno x segundo
            timestamp = getDateTime();                                    // obtener fecha y hora
            raw_val = analogRead(A0);                                     // Get ADC value 
            //float resistance = getResistance(raw_val, SERIES_RESISTOR); // convert to resistance
            
            // Print screen
            Serial.print("\t"+timestamp+"\t");
            //Serial.print(resistance);
            Serial.print("\t");
            Serial.print(raw_val);
            Serial.print("\t");
            Serial.print(h);
            Serial.println();
            
            // Write file    
            File fwlevel = SD.open(fn, FILE_WRITE);
            fwlevel.print(timestamp+"\t");
            //fwlevel.print(resistance);
            fwlevel.print("\t");  
            fwlevel.print(raw_val);
            fwlevel.print("\t");  
            fwlevel.print(h);
            fwlevel.println();
            fwlevel.close();
            
            delay(1000);
        }
    
    }else if (op == 2){
        timestamp = getDateTime();                            // obtener fecha y hora
        raw_val = analogRead(A0);                             // Get ADC value 
            
        if (raw_val >= 738){  // nivel inferior a 15cm aprox
            digitalWrite(3, LOW);
            Serial.print("Bomba ON");
            raw_val = analogRead(A0);
            while (raw_val >= 693){                          // mantengo atado al while hasta
                raw_val = analogRead(A0);                    // que supere nivel de 20cm -> 690 
                delay(500);
                Serial.print("Nivel: ");
                Serial.print(raw_val);
                Serial.println(" Llenando...");
            }
            digitalWrite(3, HIGH);                            // alcanzo 25 cm, apago bomba
            Serial.print("Nivel: ");
            Serial.print(raw_val);
            Serial.println("Bomba OFF");
        } else {
            // Print screen
            Serial.print("\t"+timestamp+"\t");
            Serial.print("\t");
            Serial.println(raw_val);

            
            // Write file    
            File fwlevel = SD.open(fn, FILE_WRITE);
            fwlevel.print(timestamp+"\t");
            fwlevel.print("\t");  
            fwlevel.println(raw_val);
            fwlevel.close();
        }
        delay(dt);
    }
}

float getResistance(float raw_val, int ref_resistance) {
  // Convert ADC reading to resistance.
  float resistance = (1023.0 / raw_val) - 1.0;
  resistance = ref_resistance / resistance;
  return resistance;
}
  
String getDateTime(){
    // return a timestamp yyyy-mm-dd hh:mm:ss
    String dtime;
    DateTime now = RTC.now();
    dtime = String(now.year()) + "-";
    dtime += fillzero(now.month()) + "-";
    dtime += fillzero(now.day()) + " ";
    dtime += fillzero(now.hour()) + ":";
    dtime += fillzero(now.minute()) + ":";
    dtime += fillzero(now.second());  
    return dtime; 
}

void getFileName(char fn[13]){
    // set fn to yyyymmdd.txt
    String dtime;
    DateTime now = RTC.now();
    dtime = String(now.year());
    dtime += fillzero(now.month());
    dtime += fillzero(now.day()) + ".txt";
    dtime.toCharArray(fn,13); 
}

String fillzero(int val){
  // return doble zero filled string
  String sval = "0";
  if (val<10)
      sval += String(val);
  else
     sval = String(val);
  return sval;
}
