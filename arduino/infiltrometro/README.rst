Automatizando un infiltrómetro de doble anillo
==============================================

Sistema para automatizar el experimento de infiltración de doble anillo, comunmente utilizado para obtener la capacidad de infiltración del suelo.

- Referencias: en ref/README.rst
- Sketchs: en sketchs/
- Código: en src/

Descripción
-----------

Fundamentos
''''''''''''

El experimiento de infiltrómetro de doble anillo se realiza en forma manual para determinar cuál es la capacidad de infiltración del suelo.

Se denomina capacidad de infiltración a la máxima velocidad a la que puede penetrar el agua en un suelo en un momento dado. Para ello, se requiere que la interfase suelo-aire esté saturada ; es decir debe existir encharcamiento.

En la etapa inicial de la infiltración, el suelo tiene un bajo contenido de humedad, el flujo es controlado por la succión y la capacidad de infiltración es alta. A medida que los poros se van llenando de agua, disminuye el efecto de la succión y, en consecuencia, disminuye la capacidad de infiltración. Cuando el contenido de humedad del suelo es igual a la capacidad de campo , se alcanza un equilibrio entre la succión y la fuerza gravitatoria .

En la etapa final predomina el potencial de gravedad, la capacidad de infiltración se hace asintótica a un valor constante, igual a la conductividad hidráulica saturada del suelo, después de un tiempo relativamente corto , que en general varía de 0.25 h a 3.00 h. La succión no es capaz de retener agua adicional, el exceso de agua sobre la capacidad de campo percola hacia el acuífero o escurre en forma subsuperficial.

La capacidad final constante depende del tipo de suelo , mientras que la capacidad para tiempos menores depende además, del contenido de humedad del suelo

Determinación empírica de la curva de capacidad de infiltración
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Pasos a seguir:

#. Limpiar la cobertura vegetal del sitio a ensayar.
#. Hincar los anillos con una maza que golpee sobre un taco de madera. El anillo interior debe estar hundido como mínimo 10 cm y el exterior la mitad. El anillo exterior sirve para establecer las condiciones de borde, de modo que el flujo logrado sea completamente unidimensional.
#. Colocar la regla graduada en el anillo interior para realizar las lecturas de niveles.
#. Llenar de agua el anillo exterior (10 cm aproximadamente) y luego el interior hasta una lectura visible en la regla.
#. Una vez hecha la instalación y agregada el agua, se comienza la experiencia, poniendo en cero el cronómetro. El nivel dentro del anillo interior no debe bajar de 5 cm, para lo cual debe agregarse agua cada vez que ese límite sea alcanzado. Cuando se repone agua debe anotarse en la planilla.
#. Se realizan una o dos series de llenado para asegurar que se llega a una tasa de infiltración constante (parte asintótica de la curva dF/dt)

Sistema automático
''''''''''''''''''

Este sistema consiste en el llenado del cilindro interior en forma automática para luego ir registrando la lámina de agua infiltrada para un intervalo de tiempo dado.


Componentes
-----------

- Arduino uno r3
- etape sensor
- datalogger shield
- bomba peristáltica
- bateria 12v
- circuito rele
  
Conexionado
-----------

- Sketch para Fritzing bajo sketchs/Sketch-Etape.fzz

.. image:: img/Etape-sketch.png
        :width: 300pt

Source
------

En src/infiltrometro/infiltrometro.ino
