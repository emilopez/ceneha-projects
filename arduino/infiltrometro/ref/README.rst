
- Fatehnia et al., Automating double ring infiltrometer with an Arduino microcontroller (2015) http://www.sciencedirect.com/science/article/pii/S0016706115300513
- Di Prima, Automated single ring infiltrometer with a low-cost microcontroller circuit (2014) http://www.sciencedirect.com/science/article/pii/S0168169915002975
