#include <SD.h>
#include <Wire.h> 
#include "RTClib.h"
 
RTC_DS1307 rtc;            
 
void setup(){
  Serial.begin(9600); Wire.begin(); rtc.begin();
  if (!rtc.isrunning()) {
    Serial.println("RTC is NOT running!");
    rtc.adjust(DateTime(__DATE__, __TIME__));
  }
 
  pinMode(10, OUTPUT);      // Para la SD
 
  if (!SD.begin(10)) {
    Serial.println("SD KO");
    return;
  }
  Serial.println("SD OK.");
  delay(3000);
}
 
void loop(){
    DateTime t = rtc.now();
    File archi = SD.open("test.txt", FILE_WRITE);
    if (archi){
        archi.print(t.year()); archi.print("-");
        archi.print(t.month());archi.print("-");
        archi.print(t.day());archi.print(" ");
        archi.print(t.hour());archi.print(":");
        archi.print(t.minute());archi.print(":");
        archi.print(t.second()); 
        archi.println();
        archi.close();
        Serial.print(t.year()); Serial.print("-");
        Serial.print(t.month());Serial.print("-");
        Serial.print(t.day());Serial.print(" ");
        Serial.print(t.hour());Serial.print(":");
        Serial.print(t.minute());Serial.print(":");
        Serial.print(t.second()); 
        Serial.println();
        
    }else
      Serial.println("error opening file");
   
    Serial.println();
    delay(5000);
}
