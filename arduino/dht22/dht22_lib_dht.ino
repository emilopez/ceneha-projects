#include <DHT.h>

#define DHTPIN 2 //Seleccionamos el pin en el que se conectará el sensor
#define DHTTYPE DHT22 //Se selecciona el DHT22 (porque hay otros DHT)
DHT dht(DHTPIN, DHTTYPE);


void setup() {
  Serial.begin(9600); //Se inicia la comunicación serial 
  dht.begin();
  delay(2000);
}
void loop() {
  //Sensor humedad y temperatura ambiente
  float h = dht.readHumidity(); //Se lee la humedad
  float t = dht.readTemperature(); //Se lee la temperatura
  //Se imprimen las variables
  Serial.print("Humedad amb.: "); 
  Serial.print(h);
  Serial.println(" %");
  Serial.print("Temperatura amb.: ");
  Serial.print(t);
  Serial.println(" C");
  Serial.println();

  delay(2000);
}

