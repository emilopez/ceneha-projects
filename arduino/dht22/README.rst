DHT22
=====

Es un sensor digital de humedad y temperatura ambiente. Mide correctamente valores de humedad entre 0 y 100% con un 2% de precisión y valores de temperatura entre -40°C y 80°C con +/- 0.5°C de precisión.
La frecuencia de muestreo de los valores no debe ser menor a 2 segundos.

Se adjunta el esquema de conexión.

.. figure:: dht22_esquema.png

Sitio web: http://www.adafruit.com/product/385 

Los pines de este sensor son:

* VCC (3.3 a 5V)
* Data
* No se conecta
* GND


