int analog_pin = 0;
int reading;
float temperatura;

void setup () {
  Serial.begin(9600);
}
void loop() {
  reading = analogRead(analog_pin);
  temperatura = 5.0*reading*100.0/1024.0;
  Serial.print("Voltaje: ");
  Serial.println(reading);
  Serial.print("Temperatura: ")
  Serial.print(temperatura);
  Serial.println(" oC");
  delay(4000);
}
