LM35
====

Descripción 
-----------

El LM35 es un sensor de temperatura, que viene calibrado con una precisión de 1ºC y es capaz de medir entre -55ºC y 150ºC. 
Tiene 3 pines de conexión. Uno es para la alimentación, otro para masa (0 Voltios) y otro para la salida de datos. 

Se adjunta el equema de conexión.

.. figure:: lm35_esquema.png

La siguiente fórmula convierte el valor obtenido del pin análogo al que se encuentra conectado nuestro LM35 a grados Celsius: 
 Celsius = (5.0 * Lectura * 100.0) / 1023 Donde Lectura es la lectura del pin analógico del Arduino conectada a la pata del centro del LM35.

Si deseamos convertir este valor a la escala Fahrenheit debemos multiplicar el valor obtenido en C por 1.8 
 Fahrenheit = Celsius * 18.0 
 
**El código de ejemplo es lm35.ino**

El lm35 es un sensor MUY sensible al ruido, como consecuencia de esto sus lecturas son poco estables.
Por este motivo se descartó su uso en el proyecto. En la figura siguiente se observan los resultados de registrar
con 3 sensores LM35 en contraste con uno digital DHT22. 

.. figure:: ruido.png
