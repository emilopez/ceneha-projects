FC28
====

Sensor de humedad cuyo funcionamiento se corresponde con la resistencia del medio donde está instalado.
Cuando la humedad aumenta, la resistencia disminuye. 

El problema de este sensor es que no tiene en cuenta el tipo de suelo, por lo que la resistencia puede ser
menor en un suelo salitroso.

El programa lee cada 2 segundos el puerto de entrada analógico A0 y escribe el valor en el puerto Serie/USB.
Se adjunta el esquema de conexión, hecho con Fritzing.

.. figure:: fc28.png

Observaciones
-------------

**Ojo, estas pruebas no fueron concluyentes, deben ser realizadas nuevamente.**

Se realizaron pruebas en laboratorio sobre este sensor comparando su funcionamiento con la sonda multiparamétrica de Stevens y observó lo siguiente:

* El FC28 se estabiliza rápidamente en un valor cuando la muestra permanece en saturación.
* Relación lineal: a mayor contenido de humedad de la muestra menor es la lectura de resistencia.
* Presenta algunos inconvenientes para estabilizarse numéricamente en un valor cuando la muestra es poco saturada, pero después de 4 hs esa oscilación disminuye a +/- 1 y podemos tomar el valor medio como verdadero. 



