
#include <SoftwareSPI2.h>


//rtc.setDateTime(DateTime(__DATE__, __TIME__));
/*
 * Prueba 6: 5 + bajo consumo
 */

#define SCK 6
#define MISO 7
#define CS 5

SoftwareSPI2 spi(SCK, MISO, CS);


void setup(){
    delay(1000);
    spi.begin();
    Serial.begin(9600);
}

void loop(){
    // despertar
   
  
    
    spi.select();
    byte firstByte = spi.transfer(0x00);
    byte secondByte = spi.transfer(0x00);
    uint16_t data = (firstByte << 8) | secondByte;
    spi.deselect();
    
    //convertido a centimeros
    float cmca = transferFunction(data)*70.4;
   
   
    Serial.println(cmca);
    
   
    delay(10);
   
  
}

float transferFunction(uint16_t dataIn) {
    float outputMax = 14746.0; // 2 to the 14th power (from sensor's data sheet)
    float outputMin = 1638.0;
    float pressureMax = 15.0; // max 30 psi (from sensor's datea sheet)
    float pressureMin = -15.0;
    // transfer function: using sensor output to solve for pressure
    float pressure = pressureMin + (dataIn - outputMin) * (pressureMax - pressureMin) / (outputMax - outputMin);
    return pressure;
}
