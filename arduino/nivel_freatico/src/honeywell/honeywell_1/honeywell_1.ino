#include <SD.h>
#include <RTClib.h>
#include <Wire.h>
#include <Honeywell_pressure_sensors.h>
#include <SPI.h>
#include <LowPower.h>

#define DLPOWER 9
#define LOGGERPIN 10

//for sensor HSCDRRN015PDSA5
HPS hps(5, 14746.0, 1638.0, 15.0, -15.0); // cs is pin 5, outputMax=14746, outputMin=1638, pressureMax=15, pressureMin=-15
RTC_DS1307 rtc;

void setup() {
    // enciendo RTC y SD
    pinMode(DLPOWER, OUTPUT);
    digitalWrite(DLPOWER, HIGH);
    pinMode(5, OUTPUT);
    digitalWrite(5, HIGH);
    
    delay(100);

    Serial.begin(9600);
    init_logger();
    
    //SPI.begin();
    //SPI.beginTransaction(SPISettings(800000, MSBFIRST, SPI_MODE0));
    
}

void loop() {
    SPI.begin();
    SPI.beginTransaction(SPISettings(800000, MSBFIRST, SPI_MODE0));
    float presion = hps.readPressure();
    
    Serial.println(F("Logger: ON"));
    digitalWrite(DLPOWER, HIGH);
    delay(100);
    init_logger();
    delay(100);
    
    char fname[13];
    String ts_rtc = timestamp_rtc();
    Serial.print(ts_rtc + " ");
    Serial.print(presion);
    
    // filename is yyyymmdd.CSV 
    String(ts_rtc.substring(0,4) + ts_rtc.substring(5,7) + ts_rtc.substring(8,10)+".CSV").toCharArray(fname,15);
    File archi = SD.open(fname, FILE_WRITE);
    if (archi){
        archi.print(ts_rtc);
        archi.print(F(";"));
        archi.println(presion);
        archi.close(); 
        Serial.println(F(": save ok"));
    }else 
        Serial.println(F(": save error"));
    
    Serial.println(F("logger: OFF"));
    digitalWrite(DLPOWER, LOW);
    //sleepXseg(1); 
    delay(500);
}

String timestamp_rtc(){
    // retorna String AAAA-MM-DD HH:MM:SS
    char ts[20];
    DateTime t = rtc.now();
    sprintf(ts, "%04d-%02d-%02d %02d:%02d:%02d",t.year(),t.month(), t.day(), t.hour(), t.minute(), t.second());
    ts[19] = '\0';
    return String(ts);
}

void sleepXseg(int cant){
  //el tiempo maximo que la libreria permite poner el Arduino en modo sleep es
  //de 8seg, por lo que se hacen varios ciclos para lograr los minutos requeridos
  //(por ej: para 10 minutos se hacen 10*60/8=75 ciclos) 
  for (int i = 0; i < cant; i++) { 
     LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF); 
  }
}

void init_logger(){
    Wire.begin();
    rtc.begin();
    delay(100);
    if (!rtc.isrunning()){
        Serial.println(F("X RTC"));
    }else Serial.println("OK RTC");
    
    if (!SD.begin(LOGGERPIN)) {
        Serial.println(F("X SD"));
    }else Serial.println("OK SD");
}
