#include <SoftwareSPI2.h>

/*
 * monitor
 */


#define SCK 6
#define MISO 7
#define CS 5

SoftwareSPI2 spi(SCK, MISO, CS);

void setup(){
  delay(1000);
  spi.begin();
  Serial.begin(9600);
}

void loop(){
  spi.select();
  byte firstByte = spi.transfer(0x00);
  byte secondByte = spi.transfer(0x00);
  uint16_t data = (firstByte << 8) | secondByte;
  spi.deselect();
  float psi = transferFunction(data);
  Serial.println(psi);
}

float transferFunction(uint16_t dataIn) {
    float outputMax = 14746.0; // 2 to the 14th power (from sensor's data sheet)
    float outputMin = 1638.0;
    float pressureMax = 15.0; // max 30 psi (from sensor's datea sheet)
    float pressureMin = -15.0;
    // transfer function: using sensor output to solve for pressure
    float pressure = pressureMin + (dataIn - outputMin) * (pressureMax - pressureMin) / (outputMax - outputMin);
    return pressure;
}
