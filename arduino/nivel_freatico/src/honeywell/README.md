# Registro nivel freático
Datalogger bajo consumo por software y hardware.

## Componentes hardware
- Datalogger Chelemino, pin 9 digital prendo/apago rtc+sd.
    - RTC DS3231 (i2c)
    - SD SPI
    - placa: Pro mini
- Sensor presión diferencial honwywell HSCDRRN015PDSA5
    - SPI

## Descripción del problema

El módulo SD no funciona con otro sensor SPI, esto se hizo en la prueba ``honeywell_1`` soluciones:

a- Usar el SPI nativo de arduino pero apagar uno cuando se usa el otro
b- SPI por software para el sensor
c- Buscar lib SD que mejore la situación

### Prueba honeywell_1

No funca para grabar porque el sensor es SPI al igual que la SD y el soft no sabe hacerlo


### Prueba honeywell_2

a- Prender/apagar el sensor usando un pin digital en estado HIGH/LOW

- Prender sensor
- Medir varios datos
- Apagar sensor
- prender SD y RTC
- guardar esos datos
- apagar SD y RTC

*No funcionó*

### Prueba 3

Usando SPI por software, lectura del sensor y salida por pantalla: *Funcionó!*

- Pro mini
- sensor en protoboard
- lib https://github.com/Flickerstrip/SoftwareSPI

Se modificó la lib para no usar MOSI, ya que el sensor tiene solo MISO.

### Prueba 4

- Prueba 3 + almacenamiento en SD: *Funcionó!*

### Prueba 5

- Prueba 4 + RTC: *Funcionó!*
- Luego con sensor en cable de 10mts: *Funcionó!*
- Luego prueba en labH con columna de agua, ver notebook jupyter del análisis

### Prueba 6

 Agrego bajo consumo:

- Apagar RTC y SD, poner a dormir por 5 minutos
- Levantar, medir, volver a dormir

*Funcionó!*

- dejar en estación meteorológica contrastando con genica en pozo real 29-08-2019




