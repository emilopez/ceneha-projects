#include <SD.h>
//#include <SoftwareSPI2.h>
#include "Sodaq_DS3231.h"
#include <DallasTemperature.h>
#include <OneWire.h> //Se importan las librerías

//rtc.setDateTime(DateTime(__DATE__, __TIME__));
/*
 * Prueba consumo
 */
 
#define SCK 6
#define MISO 7
#define CS 5
#define DL_CS 10
#define PowDL 9
#define TEMPPIN 2 //Se declara el pin donde se conectará la DATA

//SoftwareSPI2 spi(SCK, MISO, CS);

 
OneWire ourWire(TEMPPIN); //Se establece el pin declarado como bus para la comunicación OneWire
DallasTemperature dstemp(&ourWire); //Se instancia la librería DallasTemperature

File myFile;
String get_timestamp();

void setup(){
    delay(1000);
    dstemp.begin();
    //spi.begin();
    Serial.begin(9600);
    pinMode(DL_CS, OUTPUT);
    pinMode(PowDL, OUTPUT); 
    digitalWrite(PowDL,HIGH);
    delay(100);
    rtc.begin();
    delay(100);
    if (!SD.begin(CS)) {
        Serial.println("SD failed!");
    }else
        Serial.println("SD OK");
    delay(1000);
}

void loop(){
  /*
    spi.select();
    byte firstByte = spi.transfer(0x00);
    byte secondByte = spi.transfer(0x00);
    uint16_t data = (firstByte << 8) | secondByte;
    spi.deselect();
    //convertido a centimeros
    float cmca = transferFunction(data)*70.4;
  */
    String ts = get_timestamp();
    float volt = analogRead(A1) * (5 / 1023.0);
    dstemp.requestTemperatures();
    float tem = dstemp.getTempCByIndex(0);
  
    //ALMACENO 
    DateTime now = rtc.now();
    myFile = SD.open("consumo.txt", FILE_WRITE);
    delay(1000);
    if (myFile) {
        myFile.print(ts);
        myFile.print(";");
        //myFile.print(cmca);
        //myFile.print(";");
        myFile.print(tem);  
        myFile.print(";");
        myFile.println(volt);
    } else
        Serial.println("error opening file"); 
    myFile.close();
    
    
    Serial.print(ts + ";");
    //Serial.print(cmca);
    //Serial.print(";");
    Serial.print(tem);  
    Serial.print(";");
    Serial.println(volt);
    delay(1000);
}

float transferFunction(uint16_t dataIn) {
    float outputMax = 14746.0; // 2 to the 14th power (from sensor's data sheet)
    float outputMin = 1638.0;
    float pressureMax = 15.0; // max 30 psi (from sensor's datea sheet)
    float pressureMin = -15.0;
    // transfer function: using sensor output to solve for pressure
    float pressure = pressureMin + (dataIn - outputMin) * (pressureMax - pressureMin) / (outputMax - outputMin);
    return pressure;
}

String get_timestamp(){
    DateTime now = rtc.now();
    String t = String(now.year()) + "-";
    t += (now.month()  < 10)? "0" + String(now.month() )   + "-": String(now.month())  + "-";
    t += (now.date()   < 10)? "0" + String(now.date()  )   + " ": String(now.date())   + " ";
    t += (now.hour()   < 10)? "0" + String(now.hour()  )   + ":": String(now.hour())   + ":";
    t += (now.minute() < 10)? "0" + String(now.minute())   + ":": String(now.minute()) + ":";
    t += (now.second() < 10)? "0" + String(now.second())        : String(now.second());   
    return t;
}
