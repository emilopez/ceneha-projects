#include <Wire.h>
#include "Sodaq_DS3231.h"
#include <SD.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x3f, 16, 2);
const int CS = 10;
File archi;
char fn[13];
unsigned long previousMillis = 0; 
const long T = 100; 

void setup (){
    Serial.begin(9600);
    Wire.begin();
    rtc.begin();
    lcd.begin();
    //rtc.setDateTime(DateTime(__DATE__, __TIME__));
    pinMode(CS, OUTPUT);
    if (!SD.begin(CS)) {
        Serial.println("SD failed!");
        lcd.print("SD failed");
        return;
    }
    Serial.println("SD OK");
    lcd.print("SD OK");
    String fname = get_timestamp();
    fname = fname.substring(0,4)+fname.substring(5,7)+fname.substring(8,10)+".txt";
    fname.toCharArray(fn,15);
}
void loop () {
    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis >= T) {
        previousMillis = currentMillis;
        archi = SD.open(fn, FILE_WRITE);
        float dist_man, dist_sen;
        /*
        Serial.println("Ingrese distancia <enter>: ");
        while (Serial.available() == 0);
        dist_man = Serial.parseFloat();
        */
        dist_man = 100;
        //for (int i = 1; i <= 10; i++){
        String dt = get_timestamp();
        dist_sen = analogRead(A0);
        //===========
        //por serial
        //===========
        Serial.print(dt);Serial.print("\t");
        Serial.print(dist_man);Serial.print("\t");
        Serial.println(dist_sen);
        //===========
        //por display
        //===========
        lcd.setCursor(0,0);
        lcd.print(dt.substring(11));
        lcd.setCursor(10,0);
        lcd.print(dist_sen);
        lcd.setCursor(0,1);
        lcd.print(dist_man);
        //===========
        //por archivo
        //===========
        if (archi) {
            archi.print(dt);archi.print("\t");
            archi.print(dist_man);archi.print("\t");
            archi.println(dist_sen);
        } else Serial.println("error opening file");
        //}
        archi.close();
    }
}

String get_timestamp(){
    String t;
    DateTime now = rtc.now();
    t = String(now.year()) + "-";
    if (now.month()<10) 
        t+="0";
    t += String(now.month()) + "-";
    if (now.date()<10) 
        t+="0";
    t += String(now.date()) + " ";
    if (now.hour()<10) 
        t+="0";
    t += String(now.hour()) + ":";
    if (now.minute()<10) 
        t+="0";
    t += String(now.minute()) + ":";
    if (now.second()<10) 
        t+="0";
    t += String(now.second());
    return t;
}
