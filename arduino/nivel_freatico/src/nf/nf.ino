#include <Wire.h>           // Libs
#include "RTClib.h"         
#include <SD.h>
#include <Adafruit_ADS1015.h>

RTC_DS1307 RTC;
const int chipSelect = 10;
Adafruit_ADS1115 ads;  /* for the 16-bit version */
//Adafruit_ADS1015 ads;/* for the 12-bit version */

char fn[13];           // global var to filename yyyymmdd.txt
int op = 0;
int dt = 500;

void setup(){
    pinMode(chipSelect, OUTPUT);     // Para la SD
    
    Serial.begin(9600);
    Wire.begin();             
    /*
    RTC.begin();              
       
    if (! RTC.isrunning()) 
      Serial.println("RTC no funciona");
    if (!SD.begin(chipSelect)) {
      Serial.println("Error en inicializacion SD");
      Serial.println();
      return;
    }
    Serial.println("Inicializacion SD ok");
    
    getFileName(fn);                                          
    File archi;
    archi = SD.open(fn, FILE_WRITE);                         
    archi.println("Nueva Prueba: " + getDateTime());    
    archi.println("======================================");
    archi.close();
    */
    Serial.flush();                                          
    while (Serial.available() == 0);
    Serial.flush();
    
}
  
void loop(){
    String timestamp;
    float raw_val, h,vo,voltref=4.32;
    unsigned int adc0;
    double v;
    int i,j;
    float aux,vl[20];
    
    Serial.println(" Ingrese disancia/altura y presione <enter>: ");
    while (Serial.available() == 0);
    
    h = Serial.parseFloat(); //read int or parseFloat for ..float...
    Serial.print("# 10 registros x valor ingresado: ");
    Serial.print(h);
    
    for (i = 1; i <= 20; i++){
        // Por cada distancia/nivel ingresado 
        // toma 10 registros, uno x segundo
        timestamp = getDateTime();                                    
        
        //adc0 = ads.readADC_SingleEnded(0);
        //v = 5.0 * adc0 / 65535.0;
        raw_val = analogRead(0);
        vo=raw_val/1023*voltref;
        vl[i-1]=raw_val;
        // Print screen
        Serial.print("\t"+timestamp+"\t");
        Serial.print("\t");
        //Serial.print(h);
        Serial.print("\t");
        Serial.print(raw_val);
        Serial.print("\t");
        Serial.print(vo ,2);        
        Serial.print("\t");
        Serial.println();
        // Write file    
        /*
        File fwlevel = SD.open(fn, FILE_WRITE);
        fwlevel.print(timestamp+"\t");
        fwlevel.print("\t");  
        fwlevel.print(h);
        fwlevel.print("\t");          
        fwlevel.print(adc0);
        fwlevel.print("\t");  
        fwlevel.print(v,3);
        fwlevel.println();
        fwlevel.close();
        */
        delay(400);
     
    }
        for (i=0;i<19;i++)
          for (j=i+1;j<20;j++)
             if (vl[i]<vl[j]){
               aux=vl[i];
               vl[i]=vl[j];
               vl[j]=aux; }
               
        Serial.print("Mediana: ");
        Serial.print(vl[10] ,2);        
      
        Serial.print("\t");
        Serial.print(vl[10]/1023.0*voltref ,2);        
        Serial.print("\t");
        Serial.println();
  
}
  
String getDateTime(){
    // return a timestamp yyyy-mm-dd hh:mm:ss
    String dtime;
    DateTime now = RTC.now();
    dtime = String(now.year()) + "-";
    dtime += fillzero(now.month()) + "-";
    dtime += fillzero(now.day()) + " ";
    dtime += fillzero(now.hour()) + ":";
    dtime += fillzero(now.minute()) + ":";
    dtime += fillzero(now.second());  
    return dtime; 
}

void getFileName(char fn[13]){
    // set fn to yyyymmdd.txt
    String dtime;
    DateTime now = RTC.now();
    dtime = String(now.year());
    dtime += fillzero(now.month());
    dtime += fillzero(now.day()) + ".txt";
    dtime.toCharArray(fn,13); 
}

String fillzero(int val){
  // return doble zero filled string
  String sval = "0";
  if (val<10)
      sval += String(val);
  else
     sval = String(val);
  return sval;
}
