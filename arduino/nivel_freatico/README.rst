===============================================
Análisis de sensores para medir nivel freático
===============================================

.. raw:: pdf

    Spacer 200 200


:Autor:       MSc. Ing. Emiliano López
:Colaborador: Tec. Guillermo Contini - Lic. Jorge Prodoliet 
:Grupo:       Centro de Estudios HidroAmbientales (CENEHA), FICH-UNL
:Fecha: |date| |time|

.. |date| date:: %d/%m/%Y
.. |time| date:: %H:%M

.. raw:: pdf

    Spacer 0 200
    PageBreak oneColumn

.. header::
    Análisis de sensores para medir nivel freático - CENEHA

.. footer::
    Página ###Page### / ###Total###

.. Activamos el numerado de las secciones
.. sectnum::

.. contents:: Contenido


.. raw:: pdf

    Spacer 0 20


.. raw:: pdf

    PageBreak
..    SetPageCounter 1 arabic



Introducción
============

En el presente reporte se analiza el desempeño y comportamiento de diferentes sensores para registrar el nivel freático. El análisis consiste en pruebas de laboratorio y en campo para determinar los siguientes parámetros:

- Rango de medición
- Resolución
- Estabilidad

A continuación se detallan los sensores utilizados y su tecnología.

Sensores
========

Los sensores utilizados se dividen en dos categorías: distanciómetros y de presión. Los distanciómetros nos permitirán medir el nivel freático de forma indirecta, sin estar en contacto con el agua, en cambio el de presión funciona sumergido a una profundidad conocida. En la siguiente table se detallan las especificaciones de cada uno.

+--------------------------+------------+------------+------------+
| SENSOR                   | RANGO      | RESOLUCIÓN | TIPO       |
+==========================+============+============+============+
| MB7092_ XL-MaxSonar-WRMA | 0 - 765 cm | 1 cm       | acústico   |
+--------------------------+------------+------------+------------+
| MB1300_ XL-Maxsonar EZ/AE| 0 - 765 cm | 1 cm       | acústico   |
+--------------------------+------------+------------+------------+
| MB1340_ XL-Maxsonar AE4  | 0 - 765 cm | 1 cm       | acústico   |
+--------------------------+------------+------------+------------+
| MB1010_  LV-Maxsonar EZ1 | 0 - 645 cm | 1 pulgada  | acústico   |
+--------------------------+------------+------------+------------+
| MB1013_ HRLV-Maxsonar EZ | 0 - 500 cm | 1 mm       | acústico   |
+--------------------------+------------+------------+------------+
| HCSR04_                  | 2 - 450 cm | 3 mm       | acústico   |
+--------------------------+------------+------------+------------+
| Sharp GP2Y0A710K0F_      |100 - 550 cm| 2 mm       | infrarojo  |
+--------------------------+------------+------------+------------+
| Presión `analógico`_     | 0 - 1.2MPa | +/- 1,5%   | presión    |
+--------------------------+------------+------------+------------+
| Presión `digital`_       |-15 a 15 psi| +/- 2 cm   | presión    |
+--------------------------+------------+------------+------------+

.. _MB7092: http://www.maxbotix.com/Ultrasonic_Sensors/MB7092.htm
.. _MB1300: http://www.maxbotix.com/Ultrasonic_Sensors/MB1300.htm
.. _MB1340: http://www.maxbotix.com/Ultrasonic_Sensors/MB1340.htm
.. _MB1010: http://www.maxbotix.com/Ultrasonic_Sensors/MB1010.htm
.. _MB1013: http://www.maxbotix.com/Ultrasonic_Sensors/MB1013.htm
.. _HCSR04: http://www.dx.com/es/p/hc-sr04-ultrasonic-sensor-distance-measuring-module-133696
.. _GP2Y0A710K0F: https://www.adafruit.com/products/1568
.. _analógico: http://www.dx.com/es/p/carbon-steel-alloy-variable-pump-water-air-pressure-sensor-silver-black-dc-5v-343234
.. _digital: https://www.tme.eu/es/details/hscdrrn015pdsa5/sensores-de-presion/honeywell/

Distanciómetros
~~~~~~~~~~~~~~~~

Estos sensores miden la distancia entre el sensor y un objetivo, en este caso la superficie del agua. Dentro de las variantes disponibles, aquí se analizan los de tecnología infraroja y acústica.

Infrarojo
'''''''''

El sensor Sharp GP2Y0A710K0F es un distanciómetro con salida analógica compuesto por un detector de posición sensitivo (PSD, position sensitive detector), un diodo emisor de infrarojo (IRED, infrared emitting diode) y un circuito de procesamiento.

.. figure:: img/GP2Y0A710K0F.png
    :width: 700 px

    Sensor IR Sharp GP2Y0A710K0F

La salida que ofrece es analógica y varía en función de la distancia del objeto según la siguiente curva:

.. figure:: img/IR-response.png
    :width: 1300 px

    Voltaje de respuesta en función de la distancia del objeto

.. note::

    Se recomienda utilizar un capacitor de al menos 10uF en paralelo entre Vin y GND para evitar que afecte la fluctuación en la tensión.

Acústico
''''''''

Los sensores acústicos calculan la distancia a un objeto en función del tiempo de demora entre la emisión de un pulso de sonido ultrasónico y su recepción (40khz). El cálculo de la distancia se realiza utilizando la ecuación de la velocidad del sonido en el aire, modificada por la temperatura.

Dependiendo de la sofisticación del sensor en algunos casos se debe programar el cálculo y en otros simplemente leer el valor que ya computa y corrige el sensor. Además, los valores de salida puede ser en voltaje analógico, RS232 o TTL Serial, y Pulse Width.

.. figure:: img/maxbotix.png
    :width: 1700 px

    Sensores acústicos Maxbotix

**Conexionado**

El sensor MB7092 permite 2 tipos de conexiones RS232 y ANalógica. La de mayor precisión es la serie, como se observa a continuación.

.. figure:: img/conexionMB7092.png
    :width: 500 px

    Conexionado Maxbotix MB7092

Transductor de Presión Analógico
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El sensor de presión cuenta con salida analógica cuyo voltaje varía entre 0.5 - 4.5V y con una corriente de trabajo de hasta 10 mA. El valor de tensión aumenta con la presión ejercida por la columna de agua.

.. figure:: img/water-pressure4.png
    :width: 1800 px

    Dimensiones y pines del transductor de presión


Transductor de Presión Digital HONEYWELL HSCDRRN015PDSA5
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sensor de presión diferencial con salida digital mediante el protocolo SPI. Funciona a 5V con un rango de medición de ±15psi.

.. figure:: img/honywell.jpg
    :width: 1000 px

    Sensor digital diferencial de presión

Según la hoja de datos el sensor es lineal, esto se debe corroborar en la etapa de calibración. La ecuación que debe utilizarse para convertir el valor digital arrojado por el sensor a presión (psi) es la que se muestra en la siguiente Fig. 

.. figure:: img/honywell_curva.png
    :width: 1500 px

    Curva de calibración y ecuación


Pruebas realizadas
==================

Para las pruebas se utilizó un Arduino UNO o Pro Mini junto con un shield datalogger que cuenta con RTC y un módulo de memoria SD, donde se guarda un archivo CSV con los datos y pruebas realizadas. Para los sensores distanciómetros el software fue programado de tal modo que permita ingresar la distancia o altura de columna de agua a la que se encuentra alejado o sumergido el sensor y luego registre 10 valores.

Distanciómetros
~~~~~~~~~~~~~~~

Prueba 1: al aire libre
'''''''''''''''''''''''

El objetivo de esta prueba es determinar el rango de validez del sensor al aire libre. Consiste en:

- Montar los sensores sobre una mesa con ruedas
- Ir alejando de una pared a una distancia conocida

.. figure:: img/aire_libre.jpg
    :width: 700 px

    Prueba al aire libre, distancia fija.

.. figure:: img/aire_libre_reglado.jpg
    :width: 700 px

    Prueba al aire libre, distancia variable


Prueba 2: en el interior del tubo
'''''''''''''''''''''''''''''''''

El objetivo de es determinar el comportamiento del sensor dentro de un tubo de 110 mm de diámetro y contrastar con los resultados de la prueba anterior. Consiste en:

- Fijar el sensor en el extremo del tubo y acercar una distancia conocida un objeto sólido

.. figure:: img/IR_tubo.jpg
    :width: 1200 px

    Sensor infrarojo dentro del tubo

Prueba 3: en campo, estación de la FICH
'''''''''''''''''''''''''''''''''''''''

El objetivo es probar con condiciones reales, donde el rebote del sensor sea contra la superficie del agua o bien con algún objeto flotante sobre la misma.

- Fijar el sensor en la perforación de la FICH en el campus de la UNL y dejarlo registrando durante una semana
- Tomar un registro manual diariamente para luego contrastar


.. figure:: img/campus.jpg
    :width: 800 px

    Instalación en campus de la FICH

Conclusión sobre distanciómetros
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los distanciómetros dan problemas al ser introducidos dentro del tubo. Distintos efectos de rebote generan problemas por lo que no son ideales para estas circunstancias, no así al aire libre que dan una medición muy precisa y estable. Estos sensores son ideales para registrar altura de pelo de agua como un río o arroyo o bien evaporación en un tanque. 

El MB7092 es el que mejor resultados arrojó registrando adentro de un tubo, sin embargo tiene dos problemas fundamentales. Por un lado el éxito de las mediciones han sido hasta 3 metros de profundidad, por otro lado, el efecto de humedad sobre las paredes del tubo generan ruido y por ende inestabilidad en las lecturas. Este problema puede solucionarse parcialmente haciendo una ventilación en el extremo del tubo, de modo que permita salir la humedad. Es probable que se pueda mejorar el rango de medición haciendo estos arreglos.

Transductor de presión analógico
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Este sensor fue descartado por el siguiente.


Transductor de presión digital HONEYWELL HSCDRRN015PDSA5
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El avance realizado al momento consistió en las siguientes pruebas:

- Programación
- Funcionamiento / calibración
- Longitud máxima de conexionado

Prueba 1: programación
'''''''''''''''''''''''

Se desarrolló el software para comunicar el datalogger con el sensor. Para esto se utilizaron los parámetros de la hoja de datos y la biblioteca nativa de Arduino SPI. El resultado de esta prueba fue exitoso.

Prueba 2: funcionamiento / calibración inicial
''''''''''''''''''''''''''''''''''''''''''''''

Esta prueba se dividió en dos etapas. 

De desarrolló un pequeño software que arroje en pantalla las mediciones instantáneas con el fin de visualizar su comportamiento conectado a una bomba de presión regulable. El resultado de esta prueba fue exitosa, se observó la fluctuación y sensibilidad del sensor. 

En segunda instancia se dispuso una columna de agua de 1.2mts con una llave de corte. El objetivo fue contrastar los valores del sensor con mediciones manuales. Para esta prueba se empalmaron mangueras de diferente diámetro, la primera -conectada directamente al sensor- fue una sonda nasogástrica pediátrica modelo K32, de 1.4mm de diámetro interno, perfecta para conectar a la salida del sensor.

Estos empalmes son cruciales ya que una mínima pérdida puede hacer que el agua llegue a la membrana del sensor y lo dañe de forma irremediable. 

.. figure:: img/sonda.jpg
    :width: 800 px

    Sonda nasogástrica pediátrica conectada al sensor

En la calibración incial puede observarse la linealidad del sensor tal como lo indicaba su hoja de datos. 

.. figure:: img/calibracion_inicial.png
    :width: 1500 px

    Calibración inicial

Prueba 3: longitud máxima del conexionado
'''''''''''''''''''''''''''''''''''''''''

El protocolo de comunicación SPI funciona con una arquitectura maestro-esclavo, siendo el sensor el esclavo y el microcontrolador el maestro. Este protocolo fue diseñado para una distancia cercana entre ellos, por lo que es importante determinar ese límite ya que de esto dependerá el tipo de equipo a desarrollar. 

En nuestro caso se pretende registrar 10 metros de fluctuación del nivel freático por lo que necesitamos que el microcontrolador se comunique sin problemas con el sensor a esta distancia. 

Las pruebas se realizaron con cables mallados para evitar una posible interferencia externa y con pares trenzados para evitar el efecto diafonía entre los mismos cables. 

El par trenzado que se utilizó es entre la pata MISO y CLK que tienen sentidos opuestos de transmisión, buscando que de este modo se cancele el campo magnético generado por cada uno. Las pruebas realizadas fueron exitosas:


- Prueba de cable de 5 mts de longitud: exitosa
- Prueba de cable de 10 mts de longitud: exitosa

Tareas pendientes
'''''''''''''''''

Con estos resultados exitosos queda realizar las siguientes tareas y luego finalizar la calibración del sensor. 

- Encapsular el sensor de manera estanca.
- Conectar una vena de venteo que en su interior lleve los cables de conexión al sensor. 
- Loggear las mediciones que arroja el sensor dejando fija una columna de agua de 1 metro corroborando que la presión atmosférica no afecta las mediciones.
- Calibrar el sensor en un pozo real.
- Contrastar con un freatígrafo Genica.
