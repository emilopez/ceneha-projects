import glob

folder = "yacht"

files = sorted(glob.glob(folder + "/*.CSV"))
new_file_name = files[0].split(".")[0][-8:] + "-" +files[-1].split(".")[0][-8:] + ".CSV"

new_file = open(new_file_name, "w")

for file in files:
    f = open(file)
    for line in f.readlines():
        new_file.write(line)
    f.close()

new_file.close()
