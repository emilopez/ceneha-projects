#include <Wire.h>
#include "Sodaq_DS3231.h"
#include <SD.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x3f, 16, 2);
const int CS = 10;
File archi;
char fn[13]="fichnf.txt";
const int fs = 30;

void setup(){
    Serial.begin(9600); Wire.begin(); rtc.begin(); lcd.begin();
    rtc.setDateTime(DateTime(__DATE__, __TIME__));
    pinMode(CS, OUTPUT);
    if (!SD.begin(CS)) {
        Serial.println("SD failed!");
        lcd.print("SD failed");
        return;
    }
    Serial.println("SD OK");
    lcd.print("SD OK");
}
  
void loop(){
    delay(5000);
    lcd.begin();
    
    int v = analogRead(0);
    String dt = get_timestamp();
    Serial.print(dt); Serial.print("\t"); Serial.println(v);
    lcd.clear();
    lcd.setCursor(0,0); 
    lcd.print(dt.substring(11)); 
    lcd.print(" "); 
    lcd.print(v);
    
    if (rtc.now().minute()%fs == 0) {
        archi = SD.open(fn, FILE_WRITE);
        if (archi) {
            archi.print(dt);archi.print("\t"); archi.println(v);
            lcd.clear();
            dt = get_timestamp();
            lcd.setCursor(0,0); lcd.print(dt.substring(11));
            lcd.print(" "); lcd.print(v);
            lcd.setCursor(0,1); lcd.print("Guardado en SD");
            Serial.println("Guardado en SD");
        } else Serial.println("error opening file");
        archi.close();
    }else {
        Serial.println("NO ingreso");    
    }
    
}

String get_timestamp(){
    String t;
    DateTime now = rtc.now();
    t = String(now.year()) + "-";
    if (now.month()<10) 
        t+="0";
    t += String(now.month()) + "-";
    if (now.date()<10) 
        t+="0";
    t += String(now.date()) + " ";
    if (now.hour()<10) 
        t+="0";
    t += String(now.hour()) + ":";
    if (now.minute()<10) 
        t+="0";
    t += String(now.minute()) + ":";
    if (now.second()<10) 
        t+="0";
    t += String(now.second());
    return t;
}
 
