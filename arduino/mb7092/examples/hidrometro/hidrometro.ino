#include "functions.h"
#include <Maxbotix.h>
#include <SD.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <RTClib.h>
//#include <DallasTemperature.h>
//#include <OneWire.h>
#include <WiFiEsp.h>

#define TEMPPIN 7
#define DISTPIN 6
#define LOGGERPIN 10

#define ALTREFE 4.30
#define ALTINST 2.0

//OneWire ourWire(TEMPPIN);
//DallasTemperature sensor_temp(&ourWire);
Maxbotix sensor_dist(DISTPIN, Maxbotix::TX, Maxbotix::XL);
LiquidCrystal_I2C lcd(0x3f, 16, 2);
RTC_DS1307 rtc;

#ifndef HAVE_HWSERIAL1
#include "RxSoftwareSerial.h"
RxSoftwareSerial Serial1(3, 2); // RX, TX
#endif

char ssid[] = "ceneha-oeste";      // your network SSID (name)
char pass[] = "nuevooeste";        // your network password

int status = WL_IDLE_STATUS;     // the Wifi radio's status

char server[] = "ceneha.herokuapp.com";

// Initialize the Ethernet client object
WiFiEspClient client;

void setup(){
    Serial.begin(9600);
    lcd.begin();
    Wire.begin();
    rtc.begin();
    //sensor_temp.begin();
    Serial1.begin(9600);
    WiFi.init(&Serial1);

    if (!rtc.isrunning()) {
        Serial.println(F("Error RTC"));
        lcd.print(F("Error RTC"));
    }
    //rtc.adjust(DateTime(__DATE__, __TIME__));
    pinMode(LOGGERPIN, OUTPUT);

    if (!SD.begin(LOGGERPIN)) {
        Serial.println(F("Error SD"));
        lcd.print(F("Error SD"));
        return;
    }
    // check for the presence of the shield----
    if (WiFi.status() == WL_NO_SHIELD) {
      Serial.println("WiFi shield not present");
      // don't continue
      while (true);
    }

    // attempt to connect to WiFi network
    while ( status != WL_CONNECTED) {
      Serial.print("Attempting to connect to WPA SSID: ");
      Serial.println(ssid);
      // Connect to WPA/WPA2 network
      status = WiFi.begin(ssid, pass);
    }
    if (client.connect(server, 80)) {
      Serial.println("Connected to server");
      // Make a HTTP request
      client.println("GET /api-reg/ HTTP/1.1");
      client.println("Host: ceneha.herokuapp.com");
      client.println("Authorization: Token c0e0681798fce3a02b2df7b96cbd2851688051d2"); // user y pass generados en base64 (user:pass)
      client.println("User-Agent: Arduino/1.0\n");
      client.println("Connection: close");
      client.println();

  }
    // ----
    Serial.println(F("Inicio OK..."));
    lcd.print(F("Inicio OK..."));

}

void loop(){
    //sensor_temp.requestTemperatures();
    show_timestamp(&lcd);
    delay(1000);
    for (byte i=0; i < 4; i++){
      //show_data(&lcd, sensor_dist.getRange(), sensor_temp.getTempCByIndex(0), ALTREFE, ALTINST);
      show_data(&lcd, 2.0, 24.5, ALTREFE, ALTINST);
      delay(1000);
    }

    if (rtc.now().minute() % 1 == 0){
        lcd.clear();
        lcd.print("guardando...");
        //save_data(sensor_dist.getRange(), sensor_temp.getTempCByIndex(0), ALTREFE, ALTINST);
        save_data(2.0, 24.5, ALTREFE, ALTINST);
    }
    delay(500);
    while (client.available()) {
        char c = client.read();
        Serial.write(c);
    }

  // if the server's disconnected, stop the client
  if (!client.connected()) {
    Serial.println();
    Serial.println("Disconnecting from server...");
    client.stop();

    // do nothing forevermore
    while (true);
  }
}

void printWifiStatus(){
  // print the SSID of the network you're attached to
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength
  long rssi = WiFi.RSSI();
  Serial.print("Signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}
