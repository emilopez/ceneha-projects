#include <WString.h>
#include "RTClib.h"
#include <SD.h>
#include <LiquidCrystal_I2C.h>

String twodigits(int val){
    if (val<10)
        return "0" + String(val);
    return String(val);
}

String timestamp(){
    // retorna string AAAA-MM-DD HH:MM:SS
    RTC_DS1307 rtc;
    String ts;
    DateTime t = rtc.now();
    ts = String(t.year())+"-";
    ts += twodigits(t.month())+"-";
    ts += twodigits(t.day())+" ";
    ts += twodigits(t.hour())+":";
    ts += twodigits(t.minute())+":";
    ts += twodigits(t.second());
    return ts;
}


void show_timestamp(LiquidCrystal_I2C *lcd){
     String ts = timestamp();
    // timestamp x lcd
    lcd->clear();
    lcd->setCursor(0,0);
    lcd->print(ts.substring(8,10) + "-" + ts.substring(5,7) + "-" + ts.substring(0,4));
    lcd->setCursor(0,1);
    lcd->print(ts.substring(11));
}

void show_data(LiquidCrystal_I2C *lcd, int dist, float temp,  float ALTREFE, float ALTINST){
   
/*
    Serial.print(ts +" ");
    Serial.print(dist);
    Serial.print("m ");
    Serial.print(ALTREFE + ALTINST - dist/100.0);
    Serial.print("m ");
    Serial.print(temp);
    Serial.println("C");
*/  
   
    lcd->clear();
    lcd->setCursor(0,0);
    lcd->print("Altura ");
    lcd->print(ALTREFE + ALTINST - dist/100.0);
    lcd->print(" m");
    lcd->setCursor(0,1);
    lcd->print("Temp ");
    lcd->print(temp,1);
    lcd->print(" C");
}

void save_data(int dist, float temp, float altura_ref, float altura_inst){
    // Usa o crea el archivo AAAMMDD.CSV y agrega el timestamp y medicion
    char fname[13];
    String sfname = timestamp();
    sfname = sfname.substring(0,4) + sfname.substring(5,7) + sfname.substring(8,10) + ".CSV";
    sfname.toCharArray(fname,15);
    File archi = SD.open(fname, FILE_WRITE);
    if (archi){
        archi.print(timestamp());
        archi.print(";");
        archi.print(dist);
        archi.print(";");
        archi.print(altura_ref+altura_inst-dist/100.0);
        archi.print(";");
        archi.println(temp);
        archi.close();    
    }else
      Serial.println("error archivo");
}
