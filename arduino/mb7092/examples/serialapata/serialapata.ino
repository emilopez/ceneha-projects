#include <SoftwareSerial.h>
SoftwareSerial mb7092(7, 8, true); // RX, TX el sensor va conectado al pin 7 digital

void setup() {
  // put your setup code here, to run once:
  mb7092.begin(9600);
  Serial.begin(9600);

}
unsigned short readSensorSerial(uint8_t length=3){
    char buffer[length];
    // flush and wait for a range reading
    int mayor = 30;
    for (byte s = 0; s<5; s++){
        mb7092.flush();
        while (!mb7092.available() || mb7092.read() != 'R');
        for (int i = 0; i < length; i++) {
            while (!mb7092.available());
            buffer[i] = mb7092.read();
        }
        if (atoi(buffer) > mayor)
            mayor = atoi(buffer);
    }
    return mayor;

}

void loop() {
    // put your main code here, to run repeatedly:
    Serial.println(readSensorSerial());
    delay(500);
}


