/*
  Instructions:
  - Lectura serie del sensor MB7092 (XL)
    * TX is digital pin 6
    * AN is analog pin A0
  - Change code below according to your model (
    * LV
    * XL: MB7092
    * HRLV 
*/
#include "Maxbotix.h"
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

Maxbotix rangeSensorTX(6, Maxbotix::TX, Maxbotix::XL);
LiquidCrystal_I2C lcd(0x3f, 16, 2);


void setup(){
  Serial.begin(9600);lcd.begin(); Wire.begin();
}

void loop(){

  // TX
  //Serial.print(rangeSensorTX.getRange()*10.0);
  //Serial.print("mm");
  //Serial.println();
  
  unsigned long start;
  start = millis();
  Serial.print("TX MEDIAN: ");
  Serial.print(rangeSensorTX.getRange());
  Serial.print("cm");
  
  lcd.setCursor(0,0);
  lcd.print(rangeSensorTX.getRange());
  lcd.print("cm");
  
  Serial.print(" - Highest Mode: ");
  Serial.print(rangeSensorTX.getSampleMode(true));
  
  Serial.print(" - Lowest Mode: ");
  Serial.print(rangeSensorTX.getSampleMode(false));
  
  Serial.print(" - Median: ");
  Serial.print(rangeSensorTX.getSampleMedian());
  
  Serial.print(" - Best: ");
  Serial.print(rangeSensorTX.getSampleBest());
  Serial.println();
  
  delay(100);
}
