#include <Datalogger.h>
#include "Maxbotix.h"
#include <LiquidCrystal_I2C.h>

#define DISTPIN 6

Datalogger nf(10,'d');
Maxbotix distaciometro(DISTPIN, Maxbotix::TX, Maxbotix::XL);
LiquidCrystal_I2C lcd(0x3f, 16, 2);

void setup() {
  Serial.begin(9600);
  lcd.begin();
  nf.begin();
  
  if (!nf.isRunning())
      Serial.println("KO");
}

void loop() {
  String dist = String(distaciometro.getRange());
  Serial.print(nf.getDateTime());
  Serial.println(";"+dist);

  lcd.clear();
  
  lcd.setCursor(0,0);
  lcd.print(nf.getDate());
  lcd.setCursor(0,1);
  lcd.print(nf.getTime());
  lcd.print(" ");
  lcd.print(dist);
  
  delay(400);
  if (nf.getMinute() % 15 == 0){
      lcd.clear();
      lcd.print("guardando..");
      delay(100);
      nf.log(dist);
  }
  delay(500);
}
