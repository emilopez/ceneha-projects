#include <WiFiEsp.h>

#ifndef HAVE_HWSERIAL1
#include "SoftwareSerial.h"
SoftwareSerial esp8266(3, 2);       // Emulate Serial1 on pins 3/2 if not present (RX, TX)
SoftwareSerial mb7092(7, 8, true);  // (RX, TX)
#endif

#define POWLOGGER 6 //Power pin


void setup(){
    //Power lines UP!
    pinMode(POWLOGGER, OUTPUT);
    digitalWrite(POWLOGGER, HIGH);
    delay(10);
    Serial.begin(9600);
    esp8266.begin(9600);  // serial for ESP module

    WiFi.init(&esp8266);  // initialize ESP module
    if (WiFi.status() == WL_NO_SHIELD) {
        Serial.println("WiFi shield not present");
        while (true);
    }
    while (WiFi.begin("YoSobreIP", "nosesiteladoy") != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    printWifiData();
    delay(500);
    mb7092.begin(9600);
}

unsigned short readSensorSerial(uint8_t length=3){
    char buffer[length];
    // flush and wait for a range reading
    int mayor = 30;
    for (byte s = 0; s<5; s++){
        mb7092.flush();
        while (!mb7092.available() || mb7092.read() != 'R');
        for (int i = 0; i < length; i++) {
            while (!mb7092.available());
            buffer[i] = mb7092.read();
        }
        if (atoi(buffer) > mayor)
            mayor = atoi(buffer);
    }
    return mayor;
}

byte intentos = 0;
void loop(){
    delay(5000);
    mb7092.listen();
    int dist = readSensorSerial();
    float temp = random(30,40);
    Serial.println("=============================");
    Serial.print("Nivel: ");
    Serial.println(dist);
    Serial.print("Temp: ");
    Serial.println(temp);
    Serial.println("=============================");

    esp8266.listen();
    WiFiEspClient web;
    try2ConnectWeb(web);
    upload(web, temp, dist);

}

void try2ConnectWeb(WiFiEspClient &web){
    // intento de conexion a la API/servidor web
    if (!web.connect("things.ubidots.com", 80)) {
        Serial.println("connection failed");
        intentos++;
        if (intentos == 2) {
            Serial.println("rebooting");
            delay(500);
            void (*reboot)(void) = 0; // Creating a function pointer to address 0 then calling it reboots the board.
            reboot();
        }
    }else
        Serial.println("Connected to server");
}

void upload(WiFiEspClient &web, float &temp, int &dist){
    // subida de datos una vez conectado a la web
    String content = "{\"nivel\": " + String(dist) + ",\"temperatura\":" + String(temp) + "}";
    web.println("POST /api/v1.6/devices/yatch-club?token=A1E-f6DEddsoJnN3XEJxaQYT2hgFhjbcNH HTTP/1.1");
    web.println("Host: things.ubidots.com");
    web.println("Accept: */*");
    web.println("Content-Length: " + String(content.length()));
    web.println("Content-Type: application/json;");
    web.println();
    web.println(content);

    unsigned long timeout = millis();
    while (web.available() == 0) {
      if (millis() - timeout > 5000) {
        Serial.println(">>> web Timeout !");
        web.stop();
        return;
      }
    }
    String out = "";
    while (web.available()) {
        char c = web.read();
        out += c;
        //Serial.write(c);
    }
    Serial.println(out);
}

void printWifiData(){
  // print your WiFi shield's IP address
  Serial.println("=============================");
  IPAddress ip = WiFi.localIP();
  Serial.println(ip);
  // print your MAC address
  byte mac[6];
  WiFi.macAddress(mac);
  char buf[20];
  sprintf(buf, "%02X:%02X:%02X:%02X:%02X:%02X", mac[5], mac[4], mac[3], mac[2], mac[1], mac[0]);
  Serial.println(buf);
  // print the received signal strength
  long rssi = WiFi.RSSI();
  Serial.print("Signal strength (RSSI): ");
  Serial.println(rssi);
  Serial.println("=============================");
}
