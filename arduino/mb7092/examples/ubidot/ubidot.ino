#include <WiFiEsp.h>
#ifndef HAVE_HWSERIAL1
#include "SoftwareSerial.h"
SoftwareSerial Serial1(3, 2); // // Emulate Serial1 on pins 3/2 if not present (RX, TX)
#endif

#define POWLOGGER 6 //Power pin  

//char ssid[] = "YoSobreIP";
//char pass[] = "nosesiteladoy";
//int status = WL_IDLE_STATUS;
//char server[] = "things.ubidots.com";

void setup(){
    //Power lines UP!
    pinMode(POWLOGGER, OUTPUT);
    digitalWrite(POWLOGGER, HIGH);
    delay(10);
    Serial.begin(9600); 
    Serial1.begin(9600);  // serial for ESP module
    WiFi.init(&Serial1);  // initialize ESP module
    // check for the presence of the shield
    if (WiFi.status() == WL_NO_SHIELD) {
        Serial.println("WiFi shield not present");
        while (true);
    }
    while ( WiFi.begin("YoSobreIP", "nosesiteladoy") != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
        //status = WiFi.begin("YoSobreIP", "nosesiteladoy");
    }
}
byte intentos = 0;
void loop(){
    delay(5000);
    WiFiEspClient client;
    
    if (!client.connect("things.ubidots.com", 80)) {
        Serial.println("connection failed");
        intentos++;
        if (intentos == 5) {
            Serial.println("rebooting");
            delay(1000);
            void (*reboot)(void) = 0; // Creating a function pointer to address 0 then calling it reboots the board.
            reboot();
        }
        return;
    }
    // Make a HTTP request
    String content = "{\"temperatura\": " + String(12.4) + "}";
    Serial.println("Connected to server");
    client.println("POST /api/v1.6/devices/yatch-club?token=A1E-f6DEddsoJnN3XEJxaQYT2hgFhjbcNH HTTP/1.1");
    client.println("Host: things.ubidots.com");
    client.println("Accept: */*");
    //client.println("X-Auth-Token: A1E-f6DEddsoJnN3XEJxaQYT2hgFhjbcNH");
    client.println("Content-Length: " + String(content.length()));
    client.println("Content-Type: application/json;");
    client.println();
    client.println(content);
    
    unsigned long timeout = millis();
    while (client.available() == 0) {
      if (millis() - timeout > 5000) {
        Serial.println(">>> Client Timeout !");
        client.stop();
        return;
      }
    }
  
    while (client.available()) {
        //char c = client.read();
        //Serial.write(c);
        String line = client.readStringUntil('\r');
        Serial.print(line);
    }
    
}
