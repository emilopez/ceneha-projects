#include <LiquidCrystal_I2C.h>

String twodigits(int val);
String timestamp();
void save_data(int dist, float temp,  float altura_ref, float altura_inst);
void show_timestamp(LiquidCrystal_I2C *lcd);
void show_data(LiquidCrystal_I2C *lcd, int dist, float temp,  float ALTREFE, float ALTINST);

