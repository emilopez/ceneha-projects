#include <WiFiEsp.h>
#include <Wire.h>
#include <SD.h>
#include <SPI.h>
#include "Sodaq_DS3231.h"
#include <LiquidCrystal_I2C.h>

#define POWLOGGER 6 //Power pin  

#ifndef HAVE_HWSERIAL1
#include "SoftwareSerial.h"
SoftwareSerial esp8266(3, 2);       // Emulate Serial1 on pins 3/2 if not present (RX, TX)
SoftwareSerial mb7092(7, 8, true);  // (RX, TX)
#endif

LiquidCrystal_I2C lcd(0x3f, 16, 2);

#define LOGGERPIN 10
#define ALTREFE 4.30
#define ALTINST 2.0

// ===========
//  Funciones
// ===========
unsigned short readSensorSerial(uint8_t length=3){
    char buffer[length];
    // flush and wait for a range reading
    int mayor = 30;
    for (byte s = 0; s<5; s++){
        mb7092.flush();
        while (!mb7092.available() || mb7092.read() != 'R');
        for (int i = 0; i < length; i++) {
            while (!mb7092.available());
            buffer[i] = mb7092.read();
        }
        if (atoi(buffer) > mayor)
            mayor = atoi(buffer);
    }
    return mayor;
}

void setup(){
    //Power lines UP!
    pinMode(POWLOGGER, OUTPUT);
    digitalWrite(POWLOGGER, HIGH);
    delay(10);
    // init services
    Serial.begin(9600); 
    esp8266.begin(9600); 
    lcd.begin(); 
    Wire.begin();
    rtc.begin();
    WiFi.init(&esp8266);  // initialize ESP module
    if (WiFi.status() == WL_NO_SHIELD) 
        Serial.println("WiFi shield not present");
    while ( WiFi.begin("YoSobreIP", "nosesiteladoy") != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    Serial.println("Start serial sensor");
    delay(500);
    mb7092.begin(9600);

    //rtc.adjust(DateTime(__DATE__, __TIME__));
    pinMode(LOGGERPIN, OUTPUT);

    if (!SD.begin(LOGGERPIN)) {
        Serial.println(F("Error SD"));
        lcd.print(F("Error SD"));
        return;
    }
}

byte intentos = 0;
void loop(){
    delay(5000);
    mb7092.listen();
    unsigned short value = readSensorSerial();

    lcd.clear();
    lcd.print("guardando...");
        
    Serial.print("Nivel: ");
    Serial.println(value);
    esp8266.listen();
    WiFiEspClient web;
    
    if (!web.connect("things.ubidots.com", 80)) {
        Serial.println("connection failed");
        intentos++;
        if (intentos == 5) {
            Serial.println("rebooting");
            delay(1000);
            void (*reboot)(void) = 0; // Creating a function pointer to address 0 then calling it reboots the board.
            reboot();
        }
    }
    
    String content = "{\"nivel\": " + String(value) + ",\"temperatura\":" + String(random(20,31)) + "}";
    Serial.println("Connected to server");
    web.println("POST /api/v1.6/devices/yatch-club?token=A1E-f6DEddsoJnN3XEJxaQYT2hgFhjbcNH HTTP/1.1");
    web.println("Host: things.ubidots.com");
    web.println("Accept: */*");
    web.println("Content-Length: " + String(content.length()));
    web.println("Content-Type: application/json;");
    web.println();
    web.println(content);
    
    unsigned long timeout = millis();
    while (web.available() == 0) {
        if (millis() - timeout > 5000) {
            Serial.println(">>> web Timeout !");
            web.stop();
            return;
        }
    }
  
    while (web.available()) {
        char c = web.read();
        Serial.write(c);
    }
    
}


