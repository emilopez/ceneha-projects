#include "Maxbotix.h"

#define DISTPIN 7

Maxbotix sensor_dist(DISTPIN, Maxbotix::TX, Maxbotix::XL);

void setup(){
    Serial.begin(9600);
}

void loop(){
    Serial.println(sensor_dist.getRange());
    delay(500);
}
