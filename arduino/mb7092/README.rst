Sensor Ultrasónico MB7092
=========================

Algunas de las características interesantes:

- Maximum range of 765 cm
- Resolution of 1 cm
- 10Hz read rate
- 42kHz Ultrasonic sensor measures distance to objects
- Read from all 3 select sensor outputs: Analog Voltage, Serial, Analog Envelope
- Operates from 3.0-5.5V
- Low 3.4mA average current requirement 

Documentación oficial:

- http://maxbotix.com/documents/XL-MaxSonar-WR_Datasheet.pdf
- http://maxbotix.com/Ultrasonic_Sensors/MB7092.htm 

Scketch base
------------

La recomendación si no se va a hacer nada sofisticado es usar la lib maxbotix. A continuación se muestra los componentes para loggear, mostrar y alimentar un dispositivo de medición.

Componentes
'''''''''''

- Sensor acústico MB7092
- Arduino UNO r3
- Shield datalogger
- Transformador de 9 v conectado a 220 v.
- Display 16x2 con adaptador i2c

Bibliotecas
'''''''''''

Para el software se utilizaron las siguientes bibliotecas:

- Maxbotix.h (https://github.com/Diaoul/arduino-Maxbotix)
- Wire.h
- LiquidCrystal_I2C.h

Conexiones
''''''''''

El sensor MB7092 permite 3 tipos de conexiones: serie (rs232 o ttl), analógica y analog envelope (**NO tiene este modelo PWM**). Según la hoja de datos el
protocolo de mejor precisión es el serie. Así que se lo utilizó con la biblioteca Maxbotix.

La instanciación básica es la siguiente:

.. code:: cpp
    
    //instancia 
    Maxbotix sensorTX(6, Maxbotix::TX, Maxbotix::XL);

Donde
    - XL hace referencia al tipo de sensor MB7092, opciones: LV, XL y HRLV 
    - TX al protocolo a utilizar serie, opciones: TX, PW, AN 
    - 6, el pin digital de arduino donde se reciben los datos del sensor


Programa base
'''''''''''''

.. code:: cpp

    #include "Maxbotix.h"
    #include <Wire.h>
    #include <LiquidCrystal_I2C.h>
    Maxbotix sensor(6, Maxbotix::TX, Maxbotix::XL);
    LiquidCrystal_I2C lcd(0x3f, 16, 2);
    void setup(){
      Serial.begin(9600);lcd.begin(); Wire.begin();}
    void loop(){
      Serial.print("TX MEDIAN: ");
      Serial.print(sensor.getRange());
      Serial.print("cm");
      lcd.setCursor(0,0);
      lcd.print(sensor.getRange());
      lcd.print(" cm");
      Serial.println();
      delay(2000);
    }


Ejemplos varios
---------------

En la carpeta examples se encuentran distintos scketchs, que a continuación son detallados.

basicserial
'''''''''''

Usa la lib https://github.com/Diaoul/arduino-Maxbotix toma la salida serial del sensor.

nf
''
Este ejemplo es el primero que se dejó probando en el campus midiendo el nivel freático. Usa funciones propias que dejan el código muy limpio. 

- Almacena en una SD cada 15 minutos
- Muestra en un display I2C la medida instantánea y avisa cuando guarda en SD

**El código usa libs propias que dejan el main mas claro, próximos desarrollos deberían arrancar basándose en este!!**

serialapata
'''''''''''
La biblioteca mencionada en el item previo funciona perfectamente pero se genera conflicto cuando es necesario usar el puerto serie con otras bibliotecas. Este conflicto nos obliga a programar a pata para extraer los datos del sensor sin usarla.

Por lo que se utiliza la lib por defecto para comunicación serial ``SoftwareSerial.h``. A partir de ahí se comunica con el sensor y se analizan los caracteres recibidos por el sensor, cuando arriba una "R" y el sensor está disponible se toman 3 mediciones para conformar el valor de distancia.

sondamanual
'''''''''''
Pequeño programa que fue desarrollado para un instrumento manual, junto con un display de 16x2 con el objetivo de probar el sensor en campo. Conectado a la salida serie, usa la biblioteca Maxbotix, y muestra los distintos estadísticos que ella provee, máxima, mínima, mediana, mejor.

No almacena.

moduloslogger
'''''''''''''

Datalogger implementado con módulos RTC (DS3231) y SD por separado, sin usar el shield datalogger.  Para el reloj se usó la lib ``Sodaq_DS3231.h`` y para el módulo de la SD ``SD.h``. En la medición del MB7092 se usó la salida analógica y un display para mostrar la medición.

Hidrómetro
''''''''''

Este ejemplo aún no es funcional, aunque en algún momento CREO que lo fue. Actualmente se está modificando para que permita enviar por WiFi (802.11) usando la placa ESP8266. Es lo que va a estar en campo registrando la altura del río y temperatura con el DS18B20. Funcionalidades serían: Sensa alguradel río, Almacena en una SD, muestra en un LCD y enviaría a la web.

ubidot/post_ubidot
''''''''''''''''''
Sube medida a ubidot usando la ESP8266 con biblioteca WiFiEsp (https://github.com/bportaluri/WiFiEsp).

Hay que tener ciertas precauciones porque:
- Se usan 2 puertos series por software, uno para el sensor MB7092 y otro para la ESP8266.
- Cuando se los configura en el setup, queda seleccionado el último que se inició. Por lo que debe seleccioanrse el indicado antes de usarse mediante el método ``.listen()``. Un ejemplo puede verse acá: https://www.arduino.cc/en/Tutorial/TwoPortReceive

En resumen y simplificado, tenemos algo así:

.. code:: cpp

    #include <WiFiEsp.h>
    #include "SoftwareSerial.h"

    SoftwareSerial esp8266(3, 2);       // Emulate Serial on pins
    SoftwareSerial mb7092(7, 8, true);  // (RX, TX)

    void setup(){
        Serial.begin(9600); 
        esp8266.begin(9600);
        // (...) 
        mb7092.begin(9600);
    }
    void loop(){
        mb7092.listen();
        int value = readSensorSerial();
        // (...)
        esp8266.listen();
        WiFiEspClient web;
    }

Fijate bien el código completo para comprender el funcionamiento.

Datos
-----

Bajo el directorio datos se encuentran los datos de la información recolectada en diferentes pruebas, a continuación una breve descripción de cada una:

nf-campus-fich
''''''''''''''

Datos sobre la prueba recontra beta del sensor para medir nivel freático en el campus de la FICH. Se contrastaron con datos manuales. La humedad condensada sobre el tubo y el sensor fueron un problema que se debe mejorar. Quedan estos datos con fines históricos.

yacht
'''''
Datos de lo que se está registrando en el Yacht Club Santa Fe. Ojo, el sensor de temperatura está mal colocado, le da el sol y por eso mide mal. La altura del Río da perfecto, en contraste con lo que mide prefectura (aunque los muchachos a veces ponen la misma medida del día previo). 

El programa ``process-yacht-data.py`` lee el directorio que se le indique, y genera un único archivo a partir de los archivos de datos diarios, cuyo nombre de salida es el rango de fechas, por ej. el archivo ``20171101-20171129.CSV`` contiene info entre el 01/11/2017 al 29/11/2017. 

Los datos de estos archivos consisten en 4 columnas: timestamp; distancia del sensor al agua; altura del río calculada; temperatura del DS18B20

fija
''''
Datos de prueba a distancia fija, para evaluar si fluctúa lo que mide el sensor y cuánto.


















