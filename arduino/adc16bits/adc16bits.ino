#include <Wire.h>
#include <Adafruit_ADS1015.h>

Adafruit_ADS1115 ads;  /* Use this for the 16-bit version */
//Adafruit_ADS1015 ads;     /* Use thi for the 12-bit version */
float ma=0,me=70000,c=0, p=0;

void setup(void) 
{
  Serial.begin(9600);
  Serial.println("Hello!");
  
  Serial.println("Getting single-ended readings from AIN0..3");
  Serial.println("ADC Range: +/- 6.144V (1 bit = 3mV/ADS1015, 0.1875mV/ADS1115)");
  
  // The ADC input range (or gain) can be changed via the following
  // functions, but be careful never to exceed VDD +0.3V max, or to
  // exceed the upper and lower limits if you adjust the input range!
  // Setting these values incorrectly may destroy your ADC!
  //                                                                ADS1015  ADS1115
  //                                                                -------  -------
  // ads.setGain(GAIN_TWOTHIRDS);  // 2/3x gain +/- 6.144V  1 bit = 3mV      0.1875mV (default)
  // ads.setGain(GAIN_ONE);        // 1x gain   +/- 4.096V  1 bit = 2mV      0.125mV
  // ads.setGain(GAIN_TWO);        // 2x gain   +/- 2.048V  1 bit = 1mV      0.0625mV
  // ads.setGain(GAIN_FOUR);       // 4x gain   +/- 1.024V  1 bit = 0.5mV    0.03125mV
  // ads.setGain(GAIN_EIGHT);      // 8x gain   +/- 0.512V  1 bit = 0.25mV   0.015625mV
  // ads.setGain(GAIN_SIXTEEN);    // 16x gain  +/- 0.256V  1 bit = 0.125mV  0.0078125mV
  
  ads.begin();
}

void loop(void) 
{
  unsigned int adc0;

  adc0 = ads.readADC_SingleEnded(0);
  double v=5.0*adc0/65535.0;
 if (c>30){  if (ma<v) ma = v;
   if (me>v) me = v;
    Serial.print(me,4);Serial.print("  ");
   Serial.print(ma,4);Serial.print("  ");
 }
 Serial.println(v,4);
 c++;
     /*
  if (c<10){
    c++;
    p+=v;
    if (ma<v) ma = v;
    if (me>v) me = v;
  }else{
    p = p/10.0;
    c = 0;
   Serial.print(me,4);Serial.print("  ");
   Serial.print(ma,4);Serial.print("  ");
   Serial.println(p,4); 
   p = 0;
   ma = 0;
   me = 70000;
  }
  */
  delay(100);
}
