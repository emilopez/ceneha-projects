Anemómetro
==========

Sensor analógico que permite medir la velocidad del viento. 

Características
---------------

* Output: 0.4V to 2V
* Testing Range: 0.5m/s to 50m/s
* Start wind speed: 0.2 m/s
* Resolution: 0.1m/s
* Accuracy: Worst case 1 meter/s
* Max Wind Speed: 70m/s 


Conversión de tensión a m/s
---------------------------

**OJO ESTOS CÁLCULOS ESTABAN MAL, PORQUE TOMABA 1024 COMO MÁXIMO VALOR, RECALCULAR**

Todo sensor analógico retorna un valor de voltaje en su salida. Arduino cuenta con un conversor AD incorporado que nos devuelve la tensión en un valor potencia de 2. Si trabajamos con 10 bits, el máximo valor será 1023 y el mínimo 0. La conversión a volts es lineal:

- 1023  => 5 volts
- 0  => 0 volts

la recta que los mapea nos da una pendiente m = 5/1023

Una vez obtenida la tensión, se debe tener en cuenta la calibración del sensor según sus especificaciones técnicas, para este caso, un valor de 0.4v significa 0 m/s y uno de 2v 32.4 m/s:

- 0.4 v => 0 m/s
- 2.0 v => 32.4 m/s

Como el sensor es lineal, se debe calcular la pendiente y el desplazamiento de esa recta para convertir de volts a m/s. Con esta recta, el voltaje leido es la variable independiente y la velocidad del viento la dependiente.

Interconexión
-------------

* Cable negro: GND
* Cable marrón: 7-24V
* Cable azul: Salida analógica

La placa Arduino proporciona una tensión de 5V y este sensor requiere entre 7 y 24 V. Para lograr esto se suministraron 9V con una batería conectada al jack de alimentación de la placa (el máximo recomendado para Arduino es 12V) y se conectó el cable marrón al pin de alimentación Vin, que proporciona la tensión máxima con la que está alimentado Arduino. 

Comparación con estación comercial Pegasus
------------------------------------------

Se comparó el registro con el de la estación comercial Pegasus instalada en el campus de la Ciudad Universitaria, perteneciente al Centro de Informaciones Meteorológicas de la FICH/UNL. 

El análisis se realizó usando python en un jupyter notebook, se puede ver haciendo click sobre el archivo: anemometro.ipynb

La comparación de los resultados puede observarse en la siguiente figura:

.. figure:: anemometro_pegasus_vs_arduino.png
