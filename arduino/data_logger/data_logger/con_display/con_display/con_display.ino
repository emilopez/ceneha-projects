#include <Wire.h>
#include "Sodaq_DS3231.h"
#include <SD.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x3f, 16, 2);
const int CS = 10;
File archi;

void setup (){
    Serial.begin(9600);
    Wire.begin();
    rtc.begin();
    lcd.begin();
    //rtc.setDateTime(DateTime(__DATE__, __TIME__));    
    pinMode(CS, OUTPUT);
    if (!SD.begin(CS)) {
        Serial.println("SD failed!");
        lcd.print("SD failed");
        return;
    }
    Serial.println("SD OK");
    lcd.print("SD OK");
}
void loop () {
    //Serial.println("loop start");
    lcd.clear();
    String dt = get_timestamp();
    archi = SD.open("test5.txt", FILE_WRITE);
    if (archi) {
        archi.print(dt);
        archi.println("\tvalor");
        Serial.print(dt);
        Serial.println("\tvalor");
        lcd.print(dt.substring(11));
        lcd.setCursor(10,0);
        lcd.print("valor");

    } else Serial.println("error opening file");
    archi.close();
    delay(1000);
}

String get_timestamp(){
    String t;
    DateTime now = rtc.now();
    t = String(now.year()) + "-";
    if (now.month()<10) 
        t+="0";
    t += String(now.month()) + "-";
    if (now.date()<10) 
        t+="0";
    t += String(now.date()) + " ";
    if (now.hour()<10) 
        t+="0";
    t += String(now.hour()) + ":";
    if (now.minute()<10) 
        t+="0";
    t += String(now.minute()) + ":";
    if (now.second()<10) 
        t+="0";
    t += String(now.second());
    return t;
}
