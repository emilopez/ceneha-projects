- I2C or UART: https://www.whiteboxes.ch/tentacle/i2c-or-uart/
- Tentacle es el shield para 4 sensores: https://www.whiteboxes.ch/tentacle/
    - jumper settings para seleccionar i2c o uart: https://www.whiteboxes.ch/tentacle/jumper-settings/
    - ejemplos de tentacle: https://github.com/whitebox-labs/tentacle
    - lecturas asincronas: https://www.whiteboxes.ch/tentacle/continuous-asynchronous-readings/

