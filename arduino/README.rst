Arduino Projects
================

Cada directorio cuenta con un ``README.rst`` donde se detalla el contenido y su organización.
En general son proyectos Arduino, con sus esquemáticos, archivos de datos, resultados de pruebas realizadas, notebooks jupyter para el análisis de los mismos, etc.



