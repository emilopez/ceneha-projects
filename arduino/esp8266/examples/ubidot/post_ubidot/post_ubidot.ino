#include <WiFiEsp.h>

// Emulate Serial1 on pins 6/7 if not present
#ifndef HAVE_HWSERIAL1
#include "SoftwareSerial.h"
SoftwareSerial Serial1(3, 2); // (RX, TX)
#endif

#define POWA_W 6 //Power pin  

char ssid[] = "ceneha-oeste";
char pass[] = "nuevooeste";
int status = WL_IDLE_STATUS;
char server[] = "things.ubidots.com";

void setup(){
    //Power lines UP!
    pinMode(POWA_W, OUTPUT);
    digitalWrite(POWA_W, HIGH);
    delay(10);
    Serial.begin(9600);
    // initialize serial for ESP module
    Serial1.begin(9600);
    // initialize ESP module
    WiFi.init(&Serial1);
    // check for the presence of the shield
    if (WiFi.status() == WL_NO_SHIELD) {
        Serial.println("WiFi shield not present");
        // don't continue
        while (true);
    }
    while ( status != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
        status = WiFi.begin(ssid, pass);
    }
    printWifiData();
}
byte intentos = 0;
void loop(){
    delay(5000);
    //String url = "/api/v1.6/devices/yatch-club/temperatura/values?token=A1E-f6DEddsoJnN3XEJxaQYT2hgFhjbcNH";
    WiFiEspClient client;
    
    if (!client.connect(server, 80)) {
        Serial.println("connection failed");
        intentos++;
        if (intentos == 5) {
            Serial.println("rebooting");
            delay(1000);
            void (*reboot)(void) = 0; // Creating a function pointer to address 0 then calling it reboots the board.
            reboot();
        }
        return;
    }
    // Make a HTTP request
    float value = random(15, 35);
    String content = "{\"temperatura\": " + String(value) + "}";
    Serial.println("Connected to server");
    client.println("POST /api/v1.6/devices/yatch-club?token=A1E-f6DEddsoJnN3XEJxaQYT2hgFhjbcNH HTTP/1.1");
    client.println("Host: things.ubidots.com");
    client.println("Accept: */*");
    //client.println("X-Auth-Token: A1E-f6DEddsoJnN3XEJxaQYT2hgFhjbcNH");
    client.println("Content-Length: " + String(content.length()));
    client.println("Content-Type: application/json;");
    client.println();
    client.println(content);
    
    unsigned long timeout = millis();
    while (client.available() == 0) {
      if (millis() - timeout > 5000) {
        Serial.println(">>> Client Timeout !");
        client.stop();
        return;
      }
    }
  
    while (client.available()) {
        //char c = client.read();
        //Serial.write(c);
        String line = client.readStringUntil('\r');
        Serial.print(line);
    }
    
}

void printWifiData(){
  // print your WiFi shield's IP address
  IPAddress ip = WiFi.localIP();
  Serial.println(ip);
  // print your MAC address
  byte mac[6];
  WiFi.macAddress(mac);
  char buf[20];
  sprintf(buf, "%02X:%02X:%02X:%02X:%02X:%02X", mac[5], mac[4], mac[3], mac[2], mac[1], mac[0]);
  Serial.println(buf);
  // print the received signal strength
  long rssi = WiFi.RSSI();
  Serial.print("Signal strength (RSSI): ");
  Serial.println(rssi);
}
