ESP8266
=======

Alimentación
------------

El principal inconveniente suele ser la corriente que necesita para funcionar
correctamente. La capacidad de entrega de corriente del Arduino UNO en los pines
de alimentación, tanto en el de 3.3v como en el de 5v, depende de su fuente de
alimentación.

Resumiendo:

The absolute maximum for any single IO pin is 40 mA (this is the maximum. You should never actually pull a full 40 mA from a pin.

- El máximo por cada pin de I/O es de 40 mA. Si lo sobrecargás, lo dañás al microcontrolador Atmel.

- El máximo en conjunto, sumando todos los pines I/O es 200 mA.

- El pin de salida de 5V:

    - ~400 mA si el Arduino UNO es alimentado por USB
    - ~900 mA si el Arduino UNO es alumentado por una fuente externa (usando el plug). Esos 900 son para una fuente de ~7V, ya que si se incrementa el voltaje el regulador sobrecaliente entonces se decrementa la corriente de entrega.

- El pin de salida de 3.3V es capaz de suministrar 150 mA. Nota: todo lo que se alimente del pin de 3.3V pasó primeramente por el regulador del de 5V, por lo tanto si tenés algún componente que consume 100mA en la salida de 3.3 tenés que descontarlo a lo entregado por la de 5V. OK?

Conexionado
-----------

- todo

Ejemplos
--------

ubidot
''''''

Este ejemplo está hecho con un datalogger hecho por nosotros, que consiste en un Promini de 3.3v. A su vez tenemos un shield también hecho por nosotros donde está la ESP8266 soldada pero aislada eléctricamente mediante dos transistores, que permiten alimentarla al poner un pin en estado alto. Esto se ve en las siguientes líneas:


.. code:: cpp
 
    #define POWA_W 6 //Power pin 

Y luego dentro de la función ``setup()``:

.. code:: cpp

  //Power lines UP!
  pinMode(POWA_W, OUTPUT);
  digitalWrite(POWA_W, HIGH);

Las líneas precedentes podrían ser obviadas si tu esquema de trabajo no es el mismo.