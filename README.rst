CENEHA Projects
===============

Documentación de los proyectos que llevo adelante en el Centro de Estudios Hidroambientales (CENEHA) perteneciente a la Facultad de Ingeniería y Ciencias Hídricas (FICH) de la Universidad Nacional del Litoral (UNL).

El repositorio está organizado en general por directorios, donde en cada uno es se encontrará un ``README.rst`` que explica sobre ese proyecto. 

Cada sub-proyecto consisten en:

- diagramas de conexionado
- descripción de los componenentes (sensores, módulos, shields, etc)
- referencia a documentación oficial
- código utilizado
- descripción de pruebas realizadas con sus archivos de datos crudos
- análisis de estos datos, probablemente con jupyter notebook
- minireportes de la combinación de todo lo anterior

Este repositorio se encuentra en constante reorganización, toda ayuda o aporte es alentado y bienvenido. A continuación una guía sobre el contenido del repositorio:

Placas
------
- Arduino UNO
- Arduino Pro Mini

Sensores
--------
Distanciómetros
'''''''''''''''
- MB7092
- Lidar Lite v3   

Módulos y shields
-----------------
Inalámbricos
''''''''''''
- nRF24L01
- esp8266

Puchero
-------
Acá van combinados de todo lo previo o aquello que no cuadre en las categorías previas.




